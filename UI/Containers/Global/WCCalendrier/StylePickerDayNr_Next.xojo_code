#tag WebStyle
WebStyle StylePickerDayNr_Next
Inherits WebStyle
	#tag WebStyleStateGroup
		text-align=center
		text-decoration=True false false false false
		border-top=1px solid AFAFAFFF
		border-left=1px solid AFAFAFFF
		border-bottom=1px solid DADADAFF
		border-right=1px solid DADADAFF
		misc-background=gradient vertical 0 FFFFFFFF 1 EEEEEEFF
		text-size=10px
		text-color=B2B2B2FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 EAEAEAFF 1 7D7D7DFF
		text-color=FFFFFFFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 F5F5F5FF 1 2B2B2BFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StylePickerDayNr_Next
#tag EndWebStyle

