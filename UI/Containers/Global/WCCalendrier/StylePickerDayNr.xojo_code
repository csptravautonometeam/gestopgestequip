#tag WebStyle
WebStyle StylePickerDayNr
Inherits WebStyle
	#tag WebStyleStateGroup
		text-align=center
		text-decoration=True false false false false
		border-top=1px solid 8C8C8CFF
		border-left=1px solid 8C8C8CFF
		border-bottom=1px solid CCCCCCFF
		border-right=1px solid CCCCCCFF
		misc-background=gradient vertical 0 F8F8F8FF 1 CECECEFF
		text-size=10px
		text-color=666666FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 E6E6E6FF 1 4C4C4CFF
		text-color=FFFFFFFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 F5F5F5FF 1 2B2B2BFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle StylePickerDayNr
#tag EndWebStyle

