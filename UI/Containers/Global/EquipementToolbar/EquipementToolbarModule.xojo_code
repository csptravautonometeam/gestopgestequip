#tag Module
Protected Module EquipementToolbarModule
	#tag Constant, Name = INSPECTIONVEHICULE, Type = String, Dynamic = True, Default = \"Inspection v\xC3\xA9hicule", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Vehicle inspection"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inspection v\xC3\xA9hicule"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Vehicle inspection"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
