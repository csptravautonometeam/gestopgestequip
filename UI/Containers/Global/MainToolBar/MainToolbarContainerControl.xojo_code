#tag WebPage
Begin WebContainer MainToolbarContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   60
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   False
   Width           =   90
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebToolbar MainToolbar
      ButtonDisabledStyle=   "0"
      ButtonStyle     =   "0"
      Cursor          =   2
      Enabled         =   True
      Height          =   60
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "3 WebToolbarFlexibleSpace FlexibleSpace1  -1 Select... 0 0 1 1	5 WebToolbarMenu ParametresButton I01haW5Ub29sQmFyTW9kdWxlLlBBUkFNRVRSRVM= 349173759 Select... 75 0 1 1"
      ItemStyle       =   "0"
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      ToggledDisabledStyle=   "0"
      ToggledStyle    =   "0"
      Top             =   0
      Vertical        =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
#tag EndWindowCode

#tag Events MainToolbar
	#tag Event
		Sub ButtonAction(Item As WebToolbarButton)
		  Select Case item.Name
		    
		    
		  Case "LogoutButton"
		    
		    // Retour à l'applicaton du Login
		    Self.Close
		    If Session.bdTechEol <> Nil Then
		      Session.bdTechEol.Close
		      Session.bdTechEol = Nil
		    End if
		    
		    //ShowURL("http://ml6Cartier01.cartierenergie.com/baseplan/ml6cartierportail/ml6cartierportailwebapp.cgi")
		    
		  Case "ParametresButton"
		    '
		    '//Vérifier qu'une inspection est sélectionnée
		    'If Session.InspectionSideBarContainerSession.InspectionListBox.ListIndex = -1 Or Session.InspectionSideBarContainerSession.InspectionListBox.RowCount = 0 Then
		    'ActivateWebDialog(Strings.MSGEMPTYINSPECTIONLIST, "I")
		    'Exit
		    'End If
		    '
		    'MainPageUser.ProjetDetailDialogMainPageUser.ShowWordDialog
		    
		  Case "RapportsButton"
		    '
		    '//Vérifier qu'une inspection est sélectionnée
		    'If Session.ProjetsSideBarContainerSession.ProjetsList.ListIndex = -1 Or Session.ProjetsSideBarContainerSession.ProjetsList.RowCount = 0 Then
		    'ActivateWebDialog(Strings.MSGEMPTYPROJECTLIST, "I")
		    'Exit
		    'End If
		    '
		    'MainPageUser.ProjetDetailDialogMainPageUser.ShowExcelDialog
		    
		    'Case "MajParametresButton"
		    '
		    '//MainPageUser.UserDialogMainPageUser.Show
		    'MainPageUser.EmployeeDetailDialogMainPageUser.Show
		    
		  End Select
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  // Create the parametre menu
		  Dim parametreMenu As New WebMenuItem
		  parametreMenu.Append(New WebMenuItem(MainToolBarModule.TYPEEQUIPEMENT))
		  parametreMenu.Append(New WebMenuItem(MainToolBarModule.LOCALISATION))
		  parametreMenu.Append(New WebMenuItem("-"))
		  parametreMenu.Append(New WebMenuItem(MainToolBarModule.STATUT))
		  
		  // Assign the menu to the parametre button
		  Dim parametreButton As WebToolbarMenu
		  parametreButton = WebToolbarMenu(Me.ItemWithName("ParametresButton"))
		  parametreButton.Menu = parametreMenu
		  
		  // Create the report menu
		  //Dim rapportMenu As New WebMenuItem
		  //rapportMenu.Append(New WebMenuItem(Strings.REPAIRREPORT))
		  //rapportMenu.Append(New WebMenuItem("-"))
		  
		  // Assign the menu to the report button
		  //Dim rapportButton As WebToolbarMenu
		  //rapportButton = WebToolbarMenu(Me.ItemWithName("RapportsButton"))
		  //rapportButton.Menu = rapportMenu
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MenuAction(Item As WebToolbarMenu, Choice As WebMenuItem)
		  Select Case item.Name
		  Case "ParametresButton"
		    Dim mainPageParametreWebDialog As ParametreWebDialog = new ParametreWebDialog
		    Session.pointeurParametreWebDialog = mainPageParametreWebDialog
		    Select Case choice.Text
		    Case MainToolBarModule.TYPEEQUIPEMENT
		      Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom = "typeequipement"
		    Case MainToolBarModule.LOCALISATION
		      Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom = "localisation"
		    Case MainToolBarModule.STATUT
		      Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom = "statut"
		    End Select
		    // Initialisation du numéro de ligne et appel de la modification des paramètres
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne = 0
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.populateParametre
		    Session.pointeurParametreWebDialog.Show
		    
		    //Case "RapportsButton"
		    //Select Case choice.Text
		    //Case Strings.REPAIRREPORT
		    //Dim testValeur As String = Session.projetStruct.rep_date
		    //If Session.projetStruct.rep_date = "" Then
		    //pointeurProbMsgModal.WLAlertLabel.Text = Strings.ERREUR + " " + Strings.AUCUNEDONNEERAPPORT
		    //pointeurProbMsgModal.Show
		    //Exit Sub
		    //End If
		    //MainPage.MainPageExportModal.generateButtonDetail(Choice.Text)
		    //MainPage.MainPageExportModal.Show
		    //End Select
		    
		    
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
