#tag Module
Protected Module MainToolBarModule
	#tag Constant, Name = ETATACTUEL, Type = String, Dynamic = True, Default = \"\xC3\x89tat actuel", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89tat actuel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Actual status"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89tat actuel"
	#tag EndConstant

	#tag Constant, Name = LOCALISATION, Type = String, Dynamic = True, Default = \"Localisation", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Localisation"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Location"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Localisation"
	#tag EndConstant

	#tag Constant, Name = PARAMETRES, Type = String, Dynamic = True, Default = \"Param\xC3\xA8tres", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Param\xC3\xA8tres"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Param\xC3\xA8tres"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Settings"
	#tag EndConstant

	#tag Constant, Name = SITE, Type = String, Dynamic = True, Default = \"Site", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Site"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Site"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Site"
	#tag EndConstant

	#tag Constant, Name = SORTIE, Type = String, Dynamic = True, Default = \"sortie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sortie"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sortie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Logout"
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Statut", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = TYPEEQUIPEMENT, Type = String, Dynamic = True, Default = \"Type d\'\xC3\xA9quipement\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Type d\'\xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Type d\'\xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment type"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
