#tag Module
Protected Module CritereModule
	#tag Constant, Name = EQUIPEMENT, Type = String, Dynamic = True, Default = \"\xC3\x89quipement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89quipement"
	#tag EndConstant

	#tag Constant, Name = FILTRES, Type = String, Dynamic = True, Default = \"Filtres", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Filtres"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Filtres"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Filters"
	#tag EndConstant

	#tag Constant, Name = NONREVISE, Type = String, Dynamic = True, Default = \"Non r\xC3\xA9vis\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Non r\xC3\xA9vis\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Not reviewed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Non r\xC3\xA9vis\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = REVISE, Type = String, Dynamic = True, Default = \"R\xC3\xA9vis\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vis\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Reviewed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vis\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = REVISION, Type = String, Dynamic = True, Default = \"R\xC3\xA9vision", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vision"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Revision"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vision"
	#tag EndConstant

	#tag Constant, Name = TYPEEQUIPEMENT, Type = String, Dynamic = True, Default = \"Type d\'\xC3\xA9quipement\r", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Type d\'\xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Type d\'\xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment type"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
