#tag Module
Protected Module InspectionVehiculeModule
	#tag Constant, Name = ANCRAGESESPACES, Type = String, Dynamic = True, Default = \"Ancrages espac\xC3\xA9s", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ancrages espac\xC3\xA9s"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ancrages espac\xC3\xA9s"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Anchor points spaced"
	#tag EndConstant

	#tag Constant, Name = ANCRAGESTOUROK, Type = String, Dynamic = True, Default = \"Ancrages dans la tour OK", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ancrages dans la tour OK"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ancrages dans la tour OK"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Uptower anchor points OK"
	#tag EndConstant

	#tag Constant, Name = ARRIMAGE, Type = String, Dynamic = True, Default = \"Arrimage", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Arrimage"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Arrimage"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Rigging"
	#tag EndConstant

	#tag Constant, Name = AUCUNEDEFECTUOSITE, Type = String, Dynamic = True, Default = \"Aucune d\xC3\xA9fectuosit\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Aucune d\xC3\xA9fectuosit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Aucune d\xC3\xA9fectuosit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No defect"
	#tag EndConstant

	#tag Constant, Name = CABLESSUSPENSIONOK, Type = String, Dynamic = True, Default = \"C\xC3\xA2bles de suspension OK", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"C\xC3\xA2bles de suspension OK"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"C\xC3\xA2bles de suspension OK"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Suspension cables OK"
	#tag EndConstant

	#tag Constant, Name = COMMENTAIRES, Type = String, Dynamic = True, Default = \"Commentaires", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Commentaires"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Commentaires"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Comments"
	#tag EndConstant

	#tag Constant, Name = COSSESCOEURMANILLES, Type = String, Dynamic = True, Default = \"Cosses-coeur et manilles", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cosses-coeur et manilles"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cosses-coeur et manilles"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lugs and shackles"
	#tag EndConstant

	#tag Constant, Name = DATE, Type = String, Dynamic = True, Default = \"Date", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date"
	#tag EndConstant

	#tag Constant, Name = DBSCHEMANAME, Type = String, Dynamic = True, Default = \"equip", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DBTABLENAME, Type = String, Dynamic = True, Default = \"inspection_vehicule", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DOMMAGESCONNUS, Type = String, Dynamic = True, Default = \"Dommages connus", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Dommages connus"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Dommages connus"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Known damages"
	#tag EndConstant

	#tag Constant, Name = ELECTRICITEOK, Type = String, Dynamic = True, Default = \"\xC3\x89lectricit\xC3\xA9 OK", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89lectricit\xC3\xA9 OK"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89lectricit\xC3\xA9 OK"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Electricity OK"
	#tag EndConstant

	#tag Constant, Name = ENREGNEPEUTPASETREDETRUIT, Type = String, Dynamic = True, Default = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Record cannot be deleted. It is still part of a database record."
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = ETATDUVEHICULE, Type = String, Dynamic = True, Default = \"\xC3\x89tat du v\xC3\xA9hicule", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89tat du v\xC3\xA9hicule"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89tat du v\xC3\xA9hicule"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Vehicle state"
	#tag EndConstant

	#tag Constant, Name = ETIQUETTESINTACTES, Type = String, Dynamic = True, Default = \"\xC3\x89tiquettes intactes", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89tiquettes intactes"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89tiquettes intactes"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Stickers intact"
	#tag EndConstant

	#tag Constant, Name = FORMULAIRE, Type = String, Dynamic = True, Default = \"Formulaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Formulaire"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Formulaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Form"
	#tag EndConstant

	#tag Constant, Name = HINTADD, Type = String, Dynamic = True, Default = \"Ajouter", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ajouter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ajouter"
	#tag EndConstant

	#tag Constant, Name = HINTLOCKUNLOCK, Type = String, Dynamic = True, Default = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lock/Unlock record"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
	#tag EndConstant

	#tag Constant, Name = HINTSUPPRIMER, Type = String, Dynamic = True, Default = \"Supprimer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
	#tag EndConstant

	#tag Constant, Name = HINTUPDATEDETAILS, Type = String, Dynamic = True, Default = \"Enregistrer les modifications.", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrer les modifications."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Save modifications."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrer les modifications."
	#tag EndConstant

	#tag Constant, Name = INSPECTEUR, Type = String, Dynamic = True, Default = \"Inspecteur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Inspecteur"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inspecteur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Inspector"
	#tag EndConstant

	#tag Constant, Name = INSPECTION, Type = String, Dynamic = True, Default = \"Inspection", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Inspection"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inspection"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Inspection"
	#tag EndConstant

	#tag Constant, Name = INSPECTIONVEHICULEPREFIX, Type = String, Dynamic = True, Default = \"inspveh", Scope = Public
	#tag EndConstant

	#tag Constant, Name = INSPECTIONVEHICULETABLENAME, Type = String, Dynamic = True, Default = \"inspection_vehicule", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MANILLESCERTIFICATION, Type = String, Dynamic = True, Default = \"Certification des manilles", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Certification des manilles"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Certification des manilles"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Shackles certification"
	#tag EndConstant

	#tag Constant, Name = MOTEURELECTRIQUEOK, Type = String, Dynamic = True, Default = \"Moteur \xC3\xA9lectrique OK", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Moteur \xC3\xA9lectrique OK"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Moteur \xC3\xA9lectrique OK"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Electric motor OK"
	#tag EndConstant

	#tag Constant, Name = NON, Type = String, Dynamic = True, Default = \"Non", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Non"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"No"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Non"
	#tag EndConstant

	#tag Constant, Name = ODOMETRE, Type = String, Dynamic = True, Default = \"Odom\xC3\xA8tre", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Odom\xC3\xA8tre"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Odometer"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Odom\xC3\xA8tre"
	#tag EndConstant

	#tag Constant, Name = OUI, Type = String, Dynamic = True, Default = \"Oui", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Oui"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Yes"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Oui"
	#tag EndConstant

	#tag Constant, Name = PLATEFORME, Type = String, Dynamic = True, Default = \"Plateforme", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Plateforme"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Plateforme"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Platform"
	#tag EndConstant

	#tag Constant, Name = PLATEFORMEOK, Type = String, Dynamic = True, Default = \"Plateforme OK", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Plateforme OK"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Plateforme OK"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Platform OK"
	#tag EndConstant

	#tag Constant, Name = PROTECTIONANTICHUTEOK, Type = String, Dynamic = True, Default = \"Protection antichute OK", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Protection antichute OK"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Protection antichute OK"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Fall protection OK"
	#tag EndConstant

	#tag Constant, Name = REPARATIONAFAIRE, Type = String, Dynamic = True, Default = \"R\xC3\xA9paration \xC3\xA0 faire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9paration \xC3\xA0 faire"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9paration \xC3\xA0 faire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Repair to do"
	#tag EndConstant

	#tag Constant, Name = REPARATIONEFFECTUEE, Type = String, Dynamic = True, Default = \"R\xC3\xA9paration effectu\xC3\xA9e", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9paration effectu\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9paration effectu\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Repair done"
	#tag EndConstant

	#tag Constant, Name = REPARATIONENSUSPEND, Type = String, Dynamic = True, Default = \"R\xC3\xA9paration en suspend", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9paration en suspend"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9paration en suspend"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Repair on hold"
	#tag EndConstant

	#tag Constant, Name = RESPONSABLEVEHICULE, Type = String, Dynamic = True, Default = \"Responsable v\xC3\xA9hicule", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Responsable v\xC3\xA9hicule"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Responsable v\xC3\xA9hicule"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Vehicle responsible"
	#tag EndConstant

	#tag Constant, Name = REVISE, Type = String, Dynamic = True, Default = \"R\xC3\xA9vis\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vis\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Reviewed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vis\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = SANGLES, Type = String, Dynamic = True, Default = \"Sangles", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sangles"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sangles"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Slings"
	#tag EndConstant

	#tag Constant, Name = SANGLESDEFECT, Type = String, Dynamic = True, Default = \"Sangles exemptes de d\xC3\xA9fectuosit\xC3\xA9s", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sangles exemptes de d\xC3\xA9fectuosit\xC3\xA9s"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sangles exemptes de d\xC3\xA9fectuosit\xC3\xA9s"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Sling protectors on every sharp edge"
	#tag EndConstant

	#tag Constant, Name = STRUCTURECHARGE, Type = String, Dynamic = True, Default = \"Structure et charge", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Structure et charge"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Structure et charge"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Structure and load"
	#tag EndConstant

	#tag Constant, Name = ZONEDOITETRENUMERIQUE, Type = String, Dynamic = True, Default = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This field must be a numeric value"
	#tag EndConstant

	#tag Constant, Name = ZONESOBLIGATOIRES, Type = String, Dynamic = True, Default = \"Required field(s)", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required field(s)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Zone(s) obligatoire(s)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required field(s)"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
