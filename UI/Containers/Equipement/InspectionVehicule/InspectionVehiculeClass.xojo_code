#tag Class
Protected Class InspectionVehiculeClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Function chargementTitreTable(inspectionVehiculeDB As PostgreSQLDatabase, table_name As String, prefix As String , table_zone As String) As String
		  Dim recordSet as RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  Dim valeurTitre As String = ""
		  
		  strSQL = "SELECT * FROM " + table_name + " WHERE " + prefix + "_nom = '" + table_zone  +"' " + _
		  "AND " + prefix + "_tri = 0  "
		  recordSet =  inspectionVehiculeDB.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    Dim lang as string = Session.Header("Accept-Language")
		    If instr(lang, "fr")>0 Then
		      valeurTitre = recordSet.Field( prefix + "_desc_fr").StringValue
		    Else
		      valeurTitre = recordSet.Field(prefix + "_desc_en").StringValue
		    End If
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		  
		  Return valeurTitre
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, INSPECTIONVEHICULETABLENAME, INSPECTIONVEHICULEPREFIX)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerSuppression(inspectionVehiculeDB As PostgreSQLDatabase, table_nom As String, value As String) As String
		  // Vérifier si l'enregistrement sélectionné peut être supprimé
		  //Dim recordSet as RecordSet
		  //Dim strSQL As String = ""
		  
		  //Select Case table_nom
		  //
		  //Case "reppale_descanomalie"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		  //Case "reppale_etat"
		  //strSQL = "SELECT * FROM reppale WHERE reppale_rep_etat = '" + value + "'"
		  //Case "reppale_gravite"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_gravite = '" + value + "'"
		  //Case "reppale_intervention"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_intervention = '" + value + "'"
		  //Case "reppale_acces"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_methode_acces = '" + value + "'"
		  //Case "reppale_pospremiere"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_premiere = '" + value + "'"
		  //Case "reppale_posseconde"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_seconde = '" + value + "'"
		  //Case "reppale_recouvrement"
		  //strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '" + value + "'"
		  //If value = "NS" Then strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '' Or  reppale_recouvrement = '" + value + "'"
		  //Case "responsable"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_responsable = '" + value + "'"
		  //Case "sitecode"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_site = '" + value + "'"
		  //Case "reppale_statut"
		  //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_statut = '" + value + "'"
		  //Case "reppale_typeanomalie"
		  //// strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		  //
		  //End Select
		  
		  //recordSet =  inspectionVehiculeDB .SQLSelect(strSQL)
		  //Dim compteur As Integer = recordSet.RecordCount
		  
		  //If compteur > 0 Then
		  //Return InspectionVehiculeModule.ENREGNEPEUTPASETREDETRUIT
		  //Else
		  Return  "Succes"
		  //End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(inspectionVehiculeDB As PostgreSQLDatabase, ByRef inspectionVehicule_tri_param As WebTextField, ByRef inspectionVehicule_cle_param As WebTextField, ByRef inspectionVehicule_desc_fr_param As WebTextField, ByRef inspectionVehicule_desc_en_param As WebTextField) As String
		  // Couper les zones à leur longeur maximum dans la BD
		  
		  inspectionVehicule_cle_param.text =  Left(inspectionVehicule_cle_param.text,30)
		  inspectionVehicule_desc_fr_param.text = Left(inspectionVehicule_desc_fr_param.text,100)
		  inspectionVehicule_desc_en_param.text = Left(inspectionVehicule_desc_en_param.text,100)
		  
		  // Vérification des zones obligatoires
		  If inspectionVehicule_tri_param.Text = "" Then inspectionVehicule_tri_param.Style = TextFieldErrorStyle
		  If inspectionVehicule_cle_param.Text = "" Then inspectionVehicule_cle_param.Style = TextFieldErrorStyle
		  If inspectionVehicule_desc_fr_param.Text = "" Then inspectionVehicule_desc_fr_param.Style = TextFieldErrorStyle
		  If inspectionVehicule_desc_en_param.Text = "" Then inspectionVehicule_desc_en_param.Style = TextFieldErrorStyle
		  
		  If inspectionVehicule_tri_param.Style = TextFieldErrorStyle Or inspectionVehicule_cle_param.Style = TextFieldErrorStyle Or inspectionVehicule_desc_fr_param.Style = TextFieldErrorStyle Or inspectionVehicule_desc_en_param.Style = TextFieldErrorStyle  Then
		    Return InspectionVehiculeModule.ZONESOBLIGATOIRES
		  End If
		  
		  // Vérification des zones numériques
		  If inspectionVehicule_tri_param.text <> "" And  IsNumeric(inspectionVehicule_tri_param.text) = False Then inspectionVehicule_tri_param.Style = TextFieldErrorStyle
		  
		  If inspectionVehicule_tri_param.Style = TextFieldErrorStyle Then
		    Return InspectionVehiculeModule.ZONEDOITETRENUMERIQUE
		  End If
		  
		  inspectionVehicule_tri_param .text = str(Format( Val(inspectionVehicule_tri_param .text), "0"))
		  
		  Return  "Succes"
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		inspveh_arrim_ancrage_ok As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_arretes_vives As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_cable_susp_ok As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_certif_manilles As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_cosscoeur_manilles As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_eling_mousq As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_esp_ancrages As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_etat_cable As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_etiquettes As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_ligne_vie As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_longueur_cable As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_manchons_cables As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_point_ancrage As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_protec_sangles As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_prot_antichute_ok As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_sangles As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_structure_charge As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_arrim_type_cable As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_aucune_defectuosite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_code_pin As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_commentaire As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_date_inspection As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_dommages_connus As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_elec_cables As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_elec_connexions As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_elec_contacts As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_elec_disjoncteur As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_elec_gen_huile As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_elec_gen_mise As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_elec_ok As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_elec_source As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_etat_vehicule As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_inspecteur_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_kilometrage As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_mot_cable As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_mot_collants As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_mot_condition As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_mot_descente As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_mot_fonction As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_mot_force As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_mot_frein As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_mot_ok As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_no_unite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_plat_assemblage As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_plat_boulons As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_plat_capacite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_plat_condition As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_plat_goupilles As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_plat_ok As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_prot_harnais As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_resp_vehicule_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		inspveh_revision As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_ancrage_ok"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_arretes_vives"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_cable_susp_ok"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_certif_manilles"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_cosscoeur_manilles"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_eling_mousq"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_esp_ancrages"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_etat_cable"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_etiquettes"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_ligne_vie"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_longueur_cable"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_manchons_cables"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_point_ancrage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_protec_sangles"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_prot_antichute_ok"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_sangles"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_structure_charge"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_arrim_type_cable"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_aucune_defectuosite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_code_pin"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_commentaire"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_date_inspection"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_dommages_connus"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_elec_cables"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_elec_connexions"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_elec_contacts"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_elec_disjoncteur"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_elec_gen_huile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_elec_gen_mise"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_elec_ok"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_elec_source"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_etat_vehicule"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_inspecteur_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_kilometrage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_mot_cable"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_mot_collants"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_mot_condition"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_mot_descente"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_mot_fonction"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_mot_force"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_mot_frein"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_mot_ok"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_no_unite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_plat_assemblage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_plat_boulons"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_plat_capacite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_plat_condition"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_plat_goupilles"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_plat_ok"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_prot_harnais"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_resp_vehicule_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="inspveh_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
