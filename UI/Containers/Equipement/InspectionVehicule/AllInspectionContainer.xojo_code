#tag WebPage
Begin WebContainer AllInspectionContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   500
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   300
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebLabel TLLockedByInspectionVehicule
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   15
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   106
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1549580287"
      TabOrder        =   0
      Text            =   "Roger St-Arneault"
      TextAlign       =   0
      Top             =   465
      VerticalCenter  =   0
      Visible         =   False
      Width           =   140
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVLockedInspectionVehicule
      AlignHorizontal =   3
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   40
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   251
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Picture         =   311943167
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   440
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub majInspectionDetail()
		  If Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.edit_mode = "Creation" Then
		    //alimListeDeroulante
		    // Assignation des propriétés aux Contrôles
		    Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.assignPropertiesToControls(Self, InspectionVehiculeModule.INSPECTIONVEHICULEPREFIX)
		  End If
		  
		  Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.assignControlToProperties(Self, InspectionVehiculeModule.INSPECTIONVEHICULEPREFIX)
		  Dim messageErreur  As String = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.writeData(Session.bdTechEol, InspectionVehiculeModule.INSPECTIONVEHICULETABLENAME, InspectionVehiculeModule.INSPECTIONVEHICULEPREFIX)
		  
		  If (messageErreur <> "Succes") Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = InspectionVehiculeModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit
		  End If
		  
		  // Réafficher la liste si les valeurs ont changé
		  If (sauvegardeInspectionDate <> Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_date_inspection _
		    Or Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.odometre <> Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_kilometrage)  _
		    And Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.edit_mode <> "Creation"  Then
		    Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.populateInspectionVehicule
		  End If
		  
		  sauvegardeInspectionDate = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_date_inspection
		  //sauvegardeInspectionVehiculeOdometre = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_kilometrage
		  
		  Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_id
		  Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.date_inspection = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_date_inspection
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.odometre = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_kilometrage
		  
		  If Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.edit_mode <> "Creation" Then
		    Dim ML6MajModalBox As New MajModal
		    ML6MajModalBox.Show
		  End If
		  
		  Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.edit_mode = "Modification"
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub traiteMAJ()
		  If validationDesZonesOK = True Then
		    majInspectionDetail
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validationDesZonesOK() As Boolean
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, True, False)
		  
		  Dim messageRetour as String = ""
		  
		  // Validation
		  //messageRetour = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sFormulaire.validerZones( _
		  //Session.bdTechEol, _
		  //formulaire_tri_TextField, _
		  //formulaire_cle_TextField ,  _
		  //formulaire_desc_fr_TextField ,  _
		  //formulaire_desc_en_TextField)
		  //
		  //If messageRetour <> "Succes" Then
		  //Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		  //pointeurProbMsgModal.WLAlertLabel.Text = messageRetour
		  //pointeurProbMsgModal.Show
		  
		  //Return False
		  //End If
		  
		  Return True
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		prefix As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeInspectionDate As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeInspectionVehiculeOdometre As String
	#tag EndProperty

	#tag Property, Flags = &h0
		tableName As String
	#tag EndProperty


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="prefix"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="tableName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
