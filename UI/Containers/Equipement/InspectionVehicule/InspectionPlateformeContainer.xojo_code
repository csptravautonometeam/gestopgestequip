#tag WebPage
Begin AllInspectionContainer InspectionPlateformeContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   500
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   300
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   500
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1804472319"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   300
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebLabel inspveh_id_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   17
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "Id"
      TextAlign       =   0
      Top             =   431
      VerticalCenter  =   0
      Visible         =   True
      Width           =   28
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebTextField inspveh_id_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   60
      Text            =   ""
      TextAlign       =   0
      Top             =   455
      Type            =   3
      VerticalCenter  =   0
      Visible         =   True
      Width           =   59
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel inspveh_date_inspection_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   47
      Text            =   "Date"
      TextAlign       =   1
      Top             =   10
      VerticalCenter  =   0
      Visible         =   True
      Width           =   37
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField inspveh_date_inspection_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   ""
      TextAlign       =   0
      Top             =   32
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel inspveh_inspecteur_nas_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   140
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   47
      Text            =   "#InspectionVehiculeModule.INSPECTEUR"
      TextAlign       =   0
      Top             =   10
      VerticalCenter  =   0
      Visible         =   True
      Width           =   64
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel inspveh_revision_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "#InspectionVehiculeModule.REVISE"
      TextAlign       =   0
      Top             =   220
      VerticalCenter  =   0
      Visible         =   True
      Width           =   79
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup inspveh_revision_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#InspectionVehiculeModule.OUI`,`True`,``,`False`,`True`	`#InspectionVehiculeModule.NON`,`True`,``,`True`,`True`"
      Left            =   160
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "0"
      TabOrder        =   62
      Top             =   222
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu inspveh_inspecteur_nas_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   140
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   63
      Text            =   ""
      Top             =   32
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel inspveh_elec_ok_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "#InspectionVehiculeModule.ELECTRICITEOK"
      TextAlign       =   0
      Top             =   70
      VerticalCenter  =   0
      Visible         =   True
      Width           =   147
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup inspveh_elec_ok_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#InspectionVehiculeModule.OUI`,`True`,``,`False`,`True`	`#InspectionVehiculeModule.NON`,`True`,``,`True`,`True`"
      Left            =   160
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "0"
      TabOrder        =   62
      Top             =   72
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup inspveh_mot_ok_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#InspectionVehiculeModule.OUI`,`True`,``,`False`,`True`	`#InspectionVehiculeModule.NON`,`True`,``,`True`,`True`"
      Left            =   160
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "0"
      TabOrder        =   62
      Top             =   182
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel inspveh_mot_ok_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "#InspectionVehiculeModule.MOTEURELECTRIQUEOK"
      TextAlign       =   0
      Top             =   180
      VerticalCenter  =   0
      Visible         =   True
      Width           =   147
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup inspveh_plat_ok_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#InspectionVehiculeModule.OUI`,`True`,``,`False`,`True`	`#InspectionVehiculeModule.NON`,`True`,``,`True`,`True`"
      Left            =   160
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "0"
      TabOrder        =   62
      Top             =   108
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel inspveh_plat_ok_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "#InspectionVehiculeModule.PLATEFORMEOK"
      TextAlign       =   0
      Top             =   106
      VerticalCenter  =   0
      Visible         =   True
      Width           =   147
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup inspveh_prot_harnais_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#InspectionVehiculeModule.OUI`,`True`,``,`False`,`True`	`#InspectionVehiculeModule.NON`,`True`,``,`True`,`True`"
      Left            =   160
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "0"
      TabOrder        =   62
      Top             =   144
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel inspveh_prot_harnais_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "#InspectionVehiculeModule.PROTECTIONANTICHUTEOK"
      TextAlign       =   0
      Top             =   142
      VerticalCenter  =   0
      Visible         =   True
      Width           =   147
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel inspveh_commentaire_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#InspectionVehiculeModule.COMMENTAIRES"
      TextAlign       =   0
      Top             =   262
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea inspveh_commentaire_TextField
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   100
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   65
      Text            =   ""
      TextAlign       =   0
      Top             =   284
      VerticalCenter  =   0
      Visible         =   True
      Width           =   281
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub listInspectionPlateformeDetail()
		  // Si le record n'est pas vérrouillé, on déverrouille pour permettre la modification
		  If Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.lockEditMode Then 
		    // Paramètre 1 = WebView
		    // Paramètre 2 = Initialisation du contenu des zones
		    // Paramètre 3 = Initialisation des styles
		    // Paramètre 4 = Verrouillage des zones
		    GenControlModule.resetControls(Self, False, False, False)
		    // Verrouiller la zone Date
		    inspveh_date_inspection_TextField.Enabled = False
		    Exit Sub
		  End If
		  
		  //inspveh_kilometrage_TextField.SetFocus
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, True, True, True)
		  
		  If Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule = Nil Then
		    Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule = new InspectionVehiculeClass()
		  End If
		  
		  If Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.edit_mode = "Creation" Then
		    Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.edit_mode = "Modification"
		    inspveh_date_inspection_TextField.Text =str(Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.date_inspection)
		    //inspveh_kilometrage_TextField.Text =Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.inspecteur
		    Exit
		  End If
		  
		  // Alimenter les listes déroulantes
		  inspveh_inspecteur_nas_PopupMenu.loadDataPrim(Session.bdTechEol, "employe", "employe_nas", "employe_nom || ' ' || employe_prenom", "employe_nom", "ASC","employe_prenom","ASC")
		  
		  // Mode Modification, Lire les données
		  Dim inspectionPlateformeRS As RecordSet = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.loadDataByField(Session.bdTechEol, InspectionVehiculeModule.inspectionVehiculeTABLENAME, _
		  "inspveh_id", str(Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id), "inspveh_id")
		  
		  If inspectionPlateformeRS <> Nil Then Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.LireDonneesBD(inspectionPlateformeRS, InspectionVehiculeModule.inspectionVehiculePREFIX)
		  
		  inspectionPlateformeRS.Close
		  inspectionPlateformeRS = Nil
		  
		  // Remplir la liste déroulante secondaire et la positionner à la bonne valeur
		  //inspectionPlateforme_cleetr_cle_PopupMenu.loadCleEtrangere(Session.bdTechEol,  InspectionVehiculeModule.inspectionPlateformeTABLENAME, InspectionVehiculeModule.inspectionPlateformePREFIX,_
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspectionPlateforme_cleetr_nom, Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.parametre_cleetr_cle)
		  
		  
		  // Sauvegarde
		  sauvegardeInspectionPlateformeDateInspection = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_date_inspection
		  
		  Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_id
		  Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.date_inspection = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_date_inspection
		  Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.inspecteur = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.inspveh_kilometrage
		  
		  // Assignation des propriétés aux Contrôles
		  Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.sInspectionVehicule.assignPropertiesToControls(Self, InspectionVehiculeModule.inspectionVehiculePREFIX)
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, False, True)
		  
		  // Verrouiller certaines zones
		  inspveh_date_inspection_TextField.Enabled = False
		  //inspveh_inspecteur_nas_TextField.Enabled = False
		  //inspveh_resp_vehicule_nas_TextField.Enabled = False
		  // Déverrouiller certaines zones
		  //inspveh_elec_ok_RadioGroup.Enabled = True
		  //inspveh_mot_ok_RadioGroup.Enabled = True
		  //inspveh_plat_ok_RadioGroup.Enabled = True
		  //inspveh_prot_harnais_RadioGroup.Enabled = True
		  //inspveh_revision_RadioGroup.Enabled = True
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private sauvegardeInspectionPlateformeDateInspection As String
	#tag EndProperty


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="prefix"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="tableName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
