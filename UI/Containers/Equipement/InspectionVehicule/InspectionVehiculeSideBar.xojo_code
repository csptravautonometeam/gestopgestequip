#tag WebPage
Begin WebContainer InspectionVehiculeSideBar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   500
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "931806350"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   300
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   500
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1804472319"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   300
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox InspectionVehiculeListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   2
      ColumnWidths    =   "30%,70%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   445
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "Date	#InspectionVehiculeModule.ODOMETRE	#InspectionVehiculeModule.DESCRIPTIONFRANCAISE	#InspectionVehiculeModule.DESCRIPTIONANGLAISE	ST"
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "0"
      TabOrder        =   -1
      Top             =   55
      VerticalCenter  =   0
      Visible         =   True
      Width           =   300
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel InspectionVehiculeLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      TextAlign       =   0
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   210
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddInspectionVehicule
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   "#InspectionVehiculeModule.HINTADD"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   110
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   688117759
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelInspectionVehicule
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#InspectionVehiculeModule.HINTSUPPRIMER"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   680198143
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVEnrInspectionVehicule
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#InspectionVehiculeModule.HINTUPDATEDETAILS"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   180
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1996267519
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TitreTableLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1056458751"
      TabOrder        =   1
      Text            =   "Titre"
      TextAlign       =   0
      Top             =   21
      VerticalCenter  =   0
      Visible         =   True
      Width           =   130
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModInspectionVehicule
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#InspectionVehiculeModule.HINTLOCKUNLOCK"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   146
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1064464383
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVRafInspectionVehicule
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   264
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   644532223
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ajouterInspectionVehicule()
		  Dim compteur As Integer
		  compteur = InspectionVehiculeListBox.RowCount  'Pour Debug
		  compteur = InspectionVehiculeListBox.LastIndex    'Pour Debug
		  compteur = InspectionVehiculeListBox.ListIndex ' Pour Debug
		  
		  //InspectionVehiculeListBox.insertRow(compteur+1, str(Self.sInspectionVehicule.inspectionVehicule_tri), InspectionVehiculeModule.NOUVELLECLE, InspectionVehiculeModule.NOUVELLEDESCRIPTIONFRANCAISE, InspectionVehiculeModule.NOUVELLEDESCRIPTIONANGLAISE, "A")
		  
		  // Faire pointer le curseur sur le nouveau projet
		  InspectionVehiculeListBox.ListIndex = InspectionVehiculeListBox.ListIndex + 1
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.no_ligne = InspectionVehiculeListBox.ListIndex
		  
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.tri = Self.sInspectionVehicule.parametre_tri
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.cle = InspectionVehiculeModule.NOUVELLECLE
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.desc_fr = InspectionVehiculeModule.NOUVELLEDESCRIPTIONFRANCAISE
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.desc_en = InspectionVehiculeModule.NOUVELLEDESCRIPTIONANGLAISE
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.statut = "A"
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.edit_mode = "Creation"
		  
		  // Création d'un nouvel enregistrement de paramètre
		  // inspectionVehicule_tri va prendre la valeur de la ligne précédente
		  //Self.sInspectionVehicule.inspectionVehicule_nom = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.table_nom
		  //Self.sInspectionVehicule.inspectionVehicule_cle = InspectionVehiculeModule.NOUVELLECLE
		  //Self.sInspectionVehicule.inspectionVehicule_desc_fr = InspectionVehiculeModule.NOUVELLEDESCRIPTIONFRANCAISE
		  //Self.sInspectionVehicule.inspectionVehicule_desc_en = InspectionVehiculeModule.NOUVELLEDESCRIPTIONANGLAISE
		  //Self.sInspectionVehicule.inspectionVehicule_statut = "A"
		  // inspectionVehicule_cleetr_nom et inspectionVehicule_cleetr_cle vont prendre la valeur de la ligne précédente
		  
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id = 0  // indique que c'est un nouvel enregistrement
		  //Self.sInspectionVehicule.inspectionVehicule_id = 0
		  //Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeContainerDialog.majInspectionVehiculeDetail
		  
		  // Renumérotation de la listbox et de la table paramètre
		  renumerotation
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  populateInspectionVehicule
		  
		  // Se mettre en mode lock
		  //implementLock
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementInspectionVehicule(idInspectionVehicule_param As Integer)
		  // Rendre invisible les informations de verrouillage
		  Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer.IVLockedInspectionVehicule.Visible = False
		  Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer.TLLockedByInspectionVehicule.Text = ""
		  Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer.TLLockedByInspectionVehicule.Visible = False
		  
		  // Si on est en verrouillage et qu'il y a changement d'enregistrement, on remet les icones pertinents actifs et on bloque l'écran Container 
		  If Self.sInspectionVehicule.lockEditMode = True Then
		    Self.sInspectionVehicule.lockEditMode = False
		    // Enlever les verrouillages existants
		    Dim message As String = Self.sInspectionVehicule.cleanUserLocks(Session.bdTechEol)
		    If  message <> "Succes" Then afficherMessage(message)
		    
		    // Rendre disponible l'ajout et la suppression
		    IVModInspectionVehicule.Picture = ModModal30x30
		    //IVAddInspectionVehicule.Visible = True
		    //IVAddInspectionVehicule.Enabled = True
		    IVDelInspectionVehicule.Visible = True
		    IVDelInspectionVehicule.Enabled = True
		    IVRafInspectionVehicule.Visible = True
		    IVRafInspectionVehicule.Enabled = True
		    IVEnrInspectionVehicule.Visible = False
		    IVEnrInspectionVehicule.Enabled = False
		  End If
		  
		  // Si la valeur de idInspectionVehicule  est 0, c'est un nouvel enregistrement, donc pas de lecture de la BD
		  If idInspectionVehicule_param  = 0 Then
		    Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.edit_mode = "Creation"
		    Exit Sub
		  End If
		  
		  Self.sInspectionVehicule.listDataByField(Session.bdTechEol, InspectionVehiculeModule.INSPECTIONVEHICULETABLENAME,InspectionVehiculeModule.INSPECTIONVEHICULEPREFIX,  _
		  "inspveh_id", str(Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id), "inspveh_date_inspection")
		  
		  // Libérer l'espace mémoire si c'est le cas
		  If Session.pointeurAllInspectionContainer <> Nil Then
		    Session.pointeurAllInspectionContainer.Close
		    Session.pointeurAllInspectionContainer = Nil
		  End If
		  
		  // Afficher s'il y a lieu le panneau inspection des véhicules
		  Dim inspveh_kilometrage As String = Self.sInspectionVehicule.inspveh_kilometrage
		  If inspveh_kilometrage <> "" Then
		    Dim inspectionVehiculeContainer As InspectionVehiculeContainer = New InspectionVehiculeContainer
		    Session.pointeurAllInspectionContainer = inspectionVehiculeContainer
		    inspectionVehiculeContainer.EmbedWithin(Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer, 0, 0, 300, 500)
		    inspectionVehiculeContainer.LockTop = True
		    inspectionVehiculeContainer.LockBottom = True
		    inspectionVehiculeContainer.LockLeft = True
		    inspectionVehiculeContainer.LockRight = True
		    sInspectionVehicule = Nil
		    sInspectionVehicule = New InspectionVehiculeClass
		    inspectionVehiculeContainer.listInspectionVehiculeDetail
		    Exit Sub
		  End If
		  
		  // Afficher s'il y a lieu le panneau arrimage
		  Dim inspveh_arrim_ancrage_ok As String = Self.sInspectionVehicule.inspveh_arrim_ancrage_ok
		  If inspveh_arrim_ancrage_ok <> "" Then
		    Dim inspectionArrimageContainer As InspectionArrimageContainer = New InspectionArrimageContainer
		    Session.pointeurAllInspectionContainer = inspectionArrimageContainer
		    inspectionArrimageContainer.EmbedWithin(Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer, 0, 0, 300, 500)
		    inspectionArrimageContainer.LockTop = True
		    inspectionArrimageContainer.LockBottom = True
		    inspectionArrimageContainer.LockLeft = True
		    inspectionArrimageContainer.LockRight = True
		    sInspectionVehicule = Nil
		    sInspectionVehicule = New InspectionVehiculeClass
		    inspectionArrimageContainer.listInspectionArrimageDetail
		    Exit Sub
		  End If
		  
		  // Afficher s'il y a lieu le panneau plateforme
		  Dim inspveh_elec_cables As String = Self.sInspectionVehicule.inspveh_elec_cables
		  If inspveh_elec_cables <> "" Then
		    Dim inspectionPlateformeContainer As InspectionPlateformeContainer = New InspectionPlateformeContainer
		    Session.pointeurAllInspectionContainer = inspectionPlateformeContainer
		    inspectionPlateformeContainer.EmbedWithin(Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer, 0, 0, 300, 500)
		    inspectionPlateformeContainer.LockTop = True
		    inspectionPlateformeContainer.LockBottom = True
		    inspectionPlateformeContainer.LockLeft = True
		    inspectionPlateformeContainer.LockRight = True
		    sInspectionVehicule = Nil
		    sInspectionVehicule = New InspectionVehiculeClass
		    inspectionPlateformeContainer.listInspectionPlateformeDetail
		    Exit Sub
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub implementLock()
		  Dim message As String = ""
		  
		  // Déterminer de quelle formulaire il s'agit
		  Dim formulaire As String = ""
		  If Self.sInspectionVehicule.inspveh_kilometrage <> "" Then
		    formulaire = "Vehicule"
		  ElseIf Self.sInspectionVehicule.inspveh_arrim_ancrage_ok <> "" Then
		    formulaire = "Arrimage"
		  Else
		    formulaire = "Plateforme"
		  End If
		  
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String = InspectionVehiculeModule.DBSCHEMANAME + "." + InspectionVehiculeModule.DBTABLENAME + "." + str(Self.sInspectionVehicule.inspveh_id)
		  Message = Self.sInspectionVehicule.check_If_Locked(Session.bdTechEol, param_recherche)
		  If  Message <> "Locked" And Message <> "Unlocked" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If message = "Locked" Then
		    Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer.IVLockedInspectionVehicule.Visible = True
		    Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer.TLLockedByInspectionVehicule.Text = Self.sInspectionVehicule.lockedBy
		    Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer.TLLockedByInspectionVehicule.Visible = True
		  End If
		  
		  //Déverrouiller l'enregistrement si on est actuellement en Verrouillage
		  If Self.sInspectionVehicule.lockEditMode = True Then
		    message = Self.sInspectionVehicule.unLockRow(Session.bdTechEol)
		    If  message <> "Succes" Then
		      afficherMessage(message)
		      Exit Sub
		    End If
		    IVModInspectionVehicule.Picture = ModModal30x30
		    // Rendre disponible l'ajout et la suppression
		    //IVAddInspectionVehicule.Visible = True
		    //IVAddInspectionVehicule.Enabled = True
		    IVDelInspectionVehicule.Visible = True
		    IVDelInspectionVehicule.Enabled = True
		    IVRafInspectionVehicule.Visible = True
		    IVRafInspectionVehicule.Enabled = True
		    // Libérer 
		    //ptrInspectionVehiculeCont.IVLockedInspectionVehicule.Visible = False
		    //ptrInspectionVehiculeCont.TLLockedByInspectionVehicule.Visible = False
		    IVEnrInspectionVehicule.Visible = False
		    IVEnrInspectionVehicule.Enabled = False
		    
		    Self.sInspectionVehicule.lockEditMode = False
		    changementInspectionVehicule(InspectionVehiculeListBox.RowTag(InspectionVehiculeListBox.ListIndex))
		    
		    Exit Sub
		  End If
		  
		  //Activer le mode Verrouillage
		  Self.sInspectionVehicule.lockEditMode = True
		  
		  //Creer les valeurs de verrouillage record ID
		  Self.sInspectionVehicule.recordID = InspectionVehiculeModule.DBSCHEMANAME + "." +InspectionVehiculeModule.DBTABLENAME+ "." + str(Self.sInspectionVehicule.inspveh_id)
		  message = Self.sInspectionVehicule.lockRow(Session.bdTechEol) 
		  If message <> "Succes" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer.IVLockedInspectionVehicule.Visible = True
		  Session.pointeurInspectionVehiculeWebDialog.InspectionAllInspectionContainer.TLLockedByInspectionVehicule.Visible = True
		  IVEnrInspectionVehicule.Enabled = Self.sInspectionVehicule.recordAvailable
		  
		  //Activer les fonctions pertinentes
		  IVModInspectionVehicule.Picture = ModModalOrange30x30
		  //IVAddInspectionVehicule.Visible = False
		  //IVAddInspectionVehicule.Enabled = False
		  IVDelInspectionVehicule.Visible = False
		  IVDelInspectionVehicule.Enabled = False
		  IVRafInspectionVehicule.Visible = False
		  IVRafInspectionVehicule.Enabled = False
		  IVEnrInspectionVehicule.Visible = True
		  IVEnrInspectionVehicule.Enabled = True
		  
		  // Débloquer les zones du container
		  Select Case formulaire
		  Case "Vehicule"
		    Dim inspectionVehiculeContainer As InspectionVehiculeContainer = New InspectionVehiculeContainer
		    inspectionVehiculeContainer = InspectionVehiculeContainer(Session.pointeurAllInspectionContainer)
		    inspectionVehiculeContainer.listInspectionVehiculeDetail
		  Case "Arrimage"
		    Dim inspectionArrimageContainer As InspectionArrimageContainer = New InspectionArrimageContainer
		    inspectionArrimageContainer = InspectionArrimageContainer(Session.pointeurAllInspectionContainer)
		    inspectionArrimageContainer.listInspectionArrimageDetail
		  Case "Plateforme"
		    Dim inspectionPlateformeContainer As InspectionPlateformeContainer = New InspectionPlateformeContainer
		    inspectionPlateformeContainer = InspectionPlateformeContainer(Session.pointeurAllInspectionContainer)
		    inspectionPlateformeContainer.listInspectionPlateformeDetail
		  End Select
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modInspectionVehiculeDetail()
		  // Déterminer de quelle formulaire il s'agit
		  If Self.sInspectionVehicule.inspveh_kilometrage <> "" Then
		    Dim inspectionVehiculeContainer As InspectionVehiculeContainer = InspectionVehiculeContainer(Session.pointeurAllInspectionContainer)
		    inspectionVehiculeContainer.traiteMAJ
		  ElseIf Self.sInspectionVehicule.inspveh_arrim_ancrage_ok <> "" Then
		    Dim inspectionArrimageContainer As InspectionArrimageContainer = InspectionArrimageContainer(Session.pointeurAllInspectionContainer)
		    inspectionArrimageContainer.traiteMAJ
		  Else
		    Dim inspectionPlateformeContainer As InspectionPlateformeContainer = InspectionPlateformeContainer(Session.pointeurAllInspectionContainer)
		    inspectionPlateformeContainer.traiteMAJ
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateInspectionVehicule()
		  If Self.sInspectionVehicule = Nil Then
		    Self.sInspectionVehicule = new InspectionVehiculeClass()
		  End If
		  
		  Dim inspectionVehiculeRS As RecordSet
		  inspectionVehiculeRS = Self.sInspectionVehicule.loadDataByFieldSort(Session.bdTechEol, InspectionVehiculeModule.INSPECTIONVEHICULETABLENAME, _
		  "inspveh_no_unite", Session.no_unite, "inspveh_id", " DESC")
		  
		  If inspectionVehiculeRS <> Nil Then
		    
		    // Changer le titre de la 2ème colonne si ce n'est pas un formulaire d'inspection de véhicule
		    If inspectionVehiculeRS.Field("inspveh_kilometrage").StringValue = "" Then
		      InspectionVehiculeListBox.Heading(1) = InspectionVehiculeModule.FORMULAIRE
		    End If
		    
		    InspectionVehiculeListBox.DeleteAllRows
		    // Afficher le titre 
		    TitreTableLabel.Text = inspectionVehiculeRS.Field("inspveh_no_unite").StringValue
		    
		    For i As Integer = 1 To inspectionVehiculeRS.RecordCount
		      If inspectionVehiculeRS.Field("inspveh_kilometrage").StringValue <> "" Then
		        InspectionVehiculeListBox.AddRow(inspectionVehiculeRS.Field("inspveh_date_inspection").StringValue, inspectionVehiculeRS.Field("inspveh_kilometrage").StringValue)
		      ElseIf inspectionVehiculeRS.Field("inspveh_arrim_ancrage_ok").StringValue <> "" Then
		        InspectionVehiculeListBox.AddRow(inspectionVehiculeRS.Field("inspveh_date_inspection").StringValue, InspectionVehiculeModule.ARRIMAGE)
		      Else
		        InspectionVehiculeListBox.AddRow(inspectionVehiculeRS.Field("inspveh_date_inspection").StringValue, InspectionVehiculeModule.PLATEFORME)
		      End If
		      
		      // Garder le numéro de ligne
		      InspectionVehiculeListBox.RowTag(InspectionVehiculeListBox.LastIndex) = i -1
		      // Garder l'id numéro du inspectionVehicule
		      InspectionVehiculeListBox.CellTag(InspectionVehiculeListBox.LastIndex,1) = inspectionVehiculeRS.Field("inspveh_id").IntegerValue
		      
		      inspectionVehiculeRS.MoveNext
		    Next
		    
		    inspectionVehiculeRS.Close
		    inspectionVehiculeRS = Nil
		    
		    // Positionnement à la ligne gardée en mémoire
		    If InspectionVehiculeListBox.ListIndex  <> Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.no_ligne And InspectionVehiculeListBox.RowCount > 0 Then
		      Dim v As Variant = InspectionVehiculeListBox.LastIndex
		      v = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.no_ligne
		      If Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.no_ligne > InspectionVehiculeListBox.LastIndex  Then Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.no_ligne = InspectionVehiculeListBox.LastIndex
		      InspectionVehiculeListBox.ListIndex  = Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.no_ligne
		      Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id = InspectionVehiculeListBox.CellTag(Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.no_ligne,1)
		    End If
		  End If
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub renumerotation()
		  Dim inspectionVehiculeRS As RecordSet
		  Dim compteur As Integer = 0
		  
		  //inspectionVehiculeRS = Self.sInspectionVehicule.loadDataByFieldCond(Session.bdTechEol,  InspectionVehiculeModule.INSPECTIONVEHICULETABLENAME, _
		  //"inspectionVehicule_nom", " = ", Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.table_nom, " And ","parametre_tri", " >= ", "0", "parametre_tri", "parametre_id")
		  //
		  //While Not inspectionVehiculeRS.EOF
		  //
		  //Self.sInspectionVehicule.LireDonneesBD(inspectionVehiculeRS, InspectionVehiculeModule.INSPECTIONVEHICULEPREFIX)
		  //Self.sInspectionVehicule.inspectionVehicule_tri = compteur
		  //Dim messageErreur  As String = Self.sInspectionVehicule.writeData(Session.bdTechEol, InspectionVehiculeModule.INSPECTIONVEHICULETABLENAME, InspectionVehiculeModule.INSPECTIONVEHICULEPREFIX)
		  //If (messageErreur <> "Succes") Then
		  //Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		  //pointeurProbMsgModal.WLAlertLabel.Text = InspectionVehiculeModule.ERREUR + " " + messageErreur
		  //pointeurProbMsgModal.Show
		  //Exit
		  //End If
		  //
		  //compteur = compteur+1
		  //inspectionVehiculeRS.MoveNext
		  //Wend
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub supInspectionVehiculeDetail()
		  Dim messageErreur As String = "Succes"
		  
		  // Vérifier si un enregistrement peut être supprimé
		  messageErreur = Self.sInspectionVehicule.validerSuppression(Session.bdTechEol, Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.table_nom, Self.sInspectionVehicule.inspveh_date_inspection)
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = InspectionVehiculeModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String = InspectionVehiculeModule.DBSCHEMANAME + "." +InspectionVehiculeModule.DBTABLENAME+ "." + str(Self.sInspectionVehicule.inspveh_id)
		  messageErreur = Self.sInspectionVehicule.check_If_Locked(Session.bdTechEol, param_recherche)
		  If  messageErreur <> "Locked" And messageErreur <> "Unlocked" Then
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If messageErreur = "Locked" Then
		    //ptrInspectionVehiculeCont.IVLockedInspectionVehicule.Visible = True
		    //ptrInspectionVehiculeCont.TLLockedByInspectionVehicule.Text = Self.sInspectionVehicule.lockedBy
		    //ptrInspectionVehiculeCont.TLLockedByInspectionVehicule.Visible = True
		    Exit Sub
		  End If
		  
		  // L'id  est contenu dans la variable Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id
		  messageErreur = Self.sInspectionVehicule.supprimerDataByField(Session.bdTechEol, InspectionVehiculeModule.INSPECTIONVEHICULETABLENAME, "inspveh_id",str(Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id))
		  
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = InspectionVehiculeModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		  Else
		    // Renuméroter et Réafficher la liste des projets
		    Self.renumerotation
		    Self.populateInspectionVehicule
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub unlock()
		  Dim lockedRowIDBis As integer = Session.sauvegardeLockedRowID
		  Dim message As String = Self.sInspectionVehicule.unLockRow(Session.bdTechEol)
		  If  message <> "Succes" Then
		    afficherMessage(message)
		  End If
		  
		  Self.sInspectionVehicule.lockEditMode = False
		  Self.Close
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		inspectionVehiculeStruct As inspectionVehiculeStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		sInspectionVehicule As InspectionVehiculeClass
	#tag EndProperty


	#tag Structure, Name = inspectionVehiculeStructure, Flags = &h0
		date_inspection As String*12
		  odometre As String*100
		  id As Integer
		  no_ligne As Integer
		  edit_mode As String*30
		  table_nom As String*30
		  statut As String*1
		inspecteur As String*100
	#tag EndStructure


#tag EndWindowCode

#tag Events InspectionVehiculeListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Rien n'a été sélectionné, réinitialiser les champs
		    //ClearProjetsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.inspectionVehiculeStruct.id = Me.CellTag(Me.ListIndex,1)
		    changementInspectionVehicule(Me.CellTag(Me.ListIndex,1))
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddInspectionVehicule
	#tag Event
		Sub MouseExit()
		  //Self.IVAddInspectionVehicule.Picture = AjouterModal30x30
		  Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		  Self.IVEnrInspectionVehicule.Picture = Enregistrer30x30
		  Self.IVModInspectionVehicule.Picture = ModModal30x30
		  Self.IVRafInspectionVehicule.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  If Me.Enabled = True Then
		    //Self.IVAddInspectionVehicule.Picture = AjouterModalTE30x30
		    Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		    Self.IVEnrInspectionVehicule.Picture = Enregistrer30x30
		    Self.IVModInspectionVehicule.Picture = ModModal30x30
		    Self.IVRafInspectionVehicule.Picture = RefreshMenu30x30
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    ajouterInspectionVehicule
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelInspectionVehicule
	#tag Event
		Sub MouseExit()
		  //Self.IVAddInspectionVehicule.Picture = AjouterModal30x30
		  Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		  Self.IVEnrInspectionVehicule.Picture = Enregistrer30x30
		  Self.IVModInspectionVehicule.Picture = ModModal30x30
		  Self.IVRafInspectionVehicule.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  If Me.Enabled = True Then
		    //Self.IVAddInspectionVehicule.Picture = AjouterModal30x30
		    Self.IVDelInspectionVehicule.Picture = DeleteModalTE30x30
		    Self.IVEnrInspectionVehicule.Picture = Enregistrer30x30
		    Self.IVModInspectionVehicule.Picture = ModModal30x30
		    Self.IVRafInspectionVehicule.Picture = RefreshMenu30x30
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    supInspectionVehiculeDetail
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVEnrInspectionVehicule
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  modInspectionVehiculeDetail
		  
		  
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  //Self.IVAddInspectionVehicule.Picture = AjouterModal30x30
		  Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		  Self.IVEnrInspectionVehicule.Picture = Enregistrer30x30
		  Self.IVModInspectionVehicule.Picture = ModModal30x30
		  Self.IVRafInspectionVehicule.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  If Me.Enabled = True Then
		    //Self.IVAddInspectionVehicule.Picture = AjouterModal30x30
		    Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		    Self.IVEnrInspectionVehicule.Picture = EnregistrerTE30x30
		    Self.IVModInspectionVehicule.Picture = ModModal30x30
		    Self.IVRafInspectionVehicule.Picture = RefreshMenu30x30
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModInspectionVehicule
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		  //Déclenchement du mode édition et déverrouillage ou verrouillage
		  If InspectionVehiculeListBox.ListIndex <> -1 And InspectionVehiculeListBox.RowCount > 0 Then
		    implementLock
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  //Self.IVAddInspectionVehicule.Picture = AjouterModal30x30
		  Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		  Self.IVEnrInspectionVehicule.Picture = Enregistrer30x30
		  Self.IVRafInspectionVehicule.Picture = RefreshMenu30x30
		  If Self.sInspectionVehicule.lockEditMode = False Then
		    Self.IVModInspectionVehicule.Picture = ModModal30x30
		  Else
		    Self.IVModInspectionVehicule.Picture = ModModalOrange30x30
		  End If
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  If Me.Enabled = True Then
		    //Self.IVAddInspectionVehicule.Picture = AjouterModal30x30
		    Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		    Self.IVEnrInspectionVehicule.Picture = Enregistrer30x30
		    Self.IVModInspectionVehicule.Picture = ModModalTE30x30
		    Self.IVRafInspectionVehicule.Picture = RefreshMenu30x30
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVRafInspectionVehicule
	#tag Event
		Sub MouseExit()
		  Me.Picture = RefreshMenu30x30
		  //IVAddInspectionVehicule.Picture = AjouterModal30x30
		  Self.IVModInspectionVehicule.Picture = ModModal30x30
		  Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		  Self.IVDelInspectionVehicule.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  If Me.Enabled = True Then
		    Me.Picture = RefreshMenuOra30x30
		    //IVAddInspectionVehicule.Picture = AjouterModal30x30
		    IVModInspectionVehicule.Picture = ModModal30x30
		    IVDelInspectionVehicule.Picture = DeleteModal30x30
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  populateInspectionVehicule
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
