#tag WebPage
Begin WebContainer FileUploaderContainerControl
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   276
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   240
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle BackgroundRectangle
      Cursor          =   0
      Enabled         =   True
      Height          =   274
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "323070804"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   238
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebFileUploader FileUploader
      AlternateRowColor=   &cEDF3FE00
      Cursor          =   0
      Enabled         =   True
      FileCount       =   0
      Height          =   182
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      Limit           =   5
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      Style           =   "221904151"
      TabOrder        =   -1
      Top             =   48
      VerticalCenter  =   0
      Visible         =   True
      Width           =   239
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel UploadLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1012360018"
      TabOrder        =   0
      Text            =   "#FileUploaderModule.DOCUMENTSATELECHARGER"
      TextAlign       =   0
      Top             =   14
      VerticalCenter  =   0
      Visible         =   True
      Width           =   160
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton BeginUploadButton
      AutoDisable     =   False
      Caption         =   "#FileUploaderModule.TELECHARGER"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Scope           =   0
      Style           =   "1675954063"
      TabOrder        =   1
      Top             =   244
      VerticalCenter  =   0
      Visible         =   True
      Width           =   200
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVQuitter
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   202
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1461350399
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub fileUploaderFileAdded(fileUploader As WebFileUploader, fileName As String)
		  // Remove any files that do not end by an accepted extension
		  
		  If LowerCase(Right(fileName, 5)) <> ".jpeg" And _
		    LowerCase(Right(fileName, 5)) <>  ".xlsx" And _
		    LowerCase(Right(fileName, 5)) <> ".docx" And _
		    LowerCase(Right(fileName, 4)) <> ".pdf" And _
		    LowerCase(Right(fileName, 4)) <> ".xls" And _
		    LowerCase(Right(fileName, 4)) <> ".doc" And _
		    LowerCase(Right(fileName, 4)) <> ".jpg" And _
		    LowerCase(Right(fileName, 4)) <> ".png" Then
		    Dim ML6probMsgBox As New ProbMsgModal
		    Dim fileTable(-1) As String = Split(fileName, ".")
		    Dim fileExtension As String= fileTable(UBound(fileTable) - 1)
		    ML6ProbMsgBox.WLAlertLabel.Text = FileUploaderModule.DOCUMENTFORMATNOTACCEPTED + " " + fileExtension
		    ML6probMsgBox.Show
		    fileUploader.RemoveFileAtIndex(fileUploader.FileCount - 1)
		  End If
		  
		  // MsgBox("Count = " + str(fileUploader.FileCount))
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub fileUploaderUploadComplete(fileUploader As WebFileUploader, files() As WebUploadedFile)
		  // When the files have been uploaded, this events fires
		  // with the Files() parameter populated with the
		  // user's files.
		  
		  // Vérifier si le répertoire Documentation existe. S'il n'existe pas, le créer
		  Dim uploadFolder As FolderItem = App.ExecutableFile.Parent.Child("Documentation")
		  #If DebugBuild Then
		    uploadFolder = App.ExecutableFile.Parent.Parent.Child("Documentation")
		  #Endif
		  If Not uploadFolder.Exists Then
		    // Création du répertoire s'il n'existe pas
		    uploadFolder.CreateAsFolder
		  End If
		  
		  // Vérifier si le répertoire correspondant au no. d'unité existe. S'il n'existe pas, le créer
		  uploadFolder = App.ExecutableFile.Parent.Child("Documentation").Child(Session.pointeurDocumentSideBar.folderNoUnite)
		  #If DebugBuild Then
		    uploadFolder = App.ExecutableFile.Parent.Parent.Child("Documentation").Child(Session.pointeurDocumentSideBar.folderNoUnite)
		  #Endif
		  If Not uploadFolder.Exists Then
		    // Création du répertoire s'il n'existe pas
		    uploadFolder.CreateAsFolder
		  End If
		  
		  // Array pour accumuler les documents téléchargées
		  Dim documentArray(-1) As String
		  Dim indexDocument As Integer = 0
		  
		  Dim outputFile As FolderItem
		  Dim output As BinaryStream
		  
		  For Each uFile As WebUploadedFile In files
		    Try
		      // Si le fichier existe déjà, Ne pas le traiter
		      outputFile = uploadFolder.Child(uFile.Name)
		      If outputFile.Exists Then
		        Dim ML6probMsgBox As New ProbMsgModal
		        ML6ProbMsgBox.WLAlertLabel.Text = FileUploaderModule.DOCUMENTALREADYEXISTS  + uFile.Name
		        ML6probMsgBox.Show
		        Continue
		      End If
		      
		      // Sauvegarde du document
		      outputFile = New FolderItem(uFile.Name)
		      output = BinaryStream.Create(outputFile, True)
		      output.Write(uFile.Data)
		      output.Close
		      
		      outputFile.CopyFileTo(uploadFolder)
		      outputFile.Delete
		      
		      // Ajout du document dans l'array
		      indexDocument = indexDocument + 1
		      Redim documentArray(indexDocument)
		      documentArray(indexDocument-1) = uFile.Name
		      
		    Catch err As UnsupportedFormatException
		      // not a document, so skip it
		      Continue
		      
		    Catch err As IOException
		      Dim ML6probMsgBox As New ProbMsgModal
		      ML6ProbMsgBox.WLAlertLabel.Text = FileUploaderModule.UPLOADFAILEDPERMISSION  + uploadFolder.NativePath
		      ML6probMsgBox.Show
		      Exit Sub
		    End Try
		    
		  Next
		  
		  // Faire ajouter dans la liste les nouveaux documents téléchargés
		  Session.pointeurDocumentSideBar.ajouterDocuments(documentArray)
		  
		  Session.pointeurDocumentSideBar.showBoutonAdd
		  Self.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub init(webView As WebView)
		  
		  EmbedWithin(webView , 526, 75, Me.Width, Me.Height)
		  LockTop = False
		  LockBottom = False
		  LockLeft = False
		  LockRight = False
		  ZIndex = 1000
		  
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		folderUploadNoUnite As String
	#tag EndProperty


#tag EndWindowCode

#tag Events FileUploader
	#tag Event
		Sub UploadComplete(Files() As WebUploadedFile)
		  // When the files have been uploaded, this events fires
		  // with the Files() parameter populated with the
		  // user's files.
		  
		  fileUploaderUploadComplete(Me, Files)
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub UploadBegin(FileCount As Integer)
		  #Pragma Unused FileCount
		  
		  // This event fires as the upload begins. In this example
		  // this information is not needed.
		End Sub
	#tag EndEvent
	#tag Event
		Sub FileAdded(Filename As String)
		  // Téléchargement des documents sélectionnés par l'utilisateur
		  fileUploaderFileAdded(Me, Filename)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BeginUploadButton
	#tag Event
		Sub Action()
		  // Calling this method will tell the uploader to begin, if the user
		  // has selected any files
		  
		  FileUploader.Upload()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVQuitter
	#tag Event
		Sub MouseExit()
		  Self.IVQuitter.Picture = FermerModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVQuitter.Picture = FermerModalOrange30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  Session.pointeurDocumentSideBar.showBoutonAdd
		  Self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="folderUploadNoUnite"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
