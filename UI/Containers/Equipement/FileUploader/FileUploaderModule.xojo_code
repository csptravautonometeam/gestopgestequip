#tag Module
Protected Module FileUploaderModule
	#tag Constant, Name = DOCUMENTALREADYEXISTS, Type = String, Dynamic = True, Default = \"Document existe d\xC3\xA9j\xC3\xA0:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Document existe d\xC3\xA9j\xC3\xA0:\r"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Document existe d\xC3\xA9j\xC3\xA0:\r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Document already exists:\r"
	#tag EndConstant

	#tag Constant, Name = DOCUMENTFORMATNOTACCEPTED, Type = String, Dynamic = True, Default = \"Format de document non accept\xC3\xA9: ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Format de document non accept\xC3\xA9: \r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Format de document non accept\xC3\xA9: \r\n"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Document format not accepted:\r\n"
	#tag EndConstant

	#tag Constant, Name = DOCUMENTSATELECHARGER, Type = String, Dynamic = True, Default = \"Documents \xC3\xA0 t\xC3\xA9l\xC3\xA9charger", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Documents \xC3\xA0 t\xC3\xA9l\xC3\xA9charger"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Documents \xC3\xA0 t\xC3\xA9l\xC3\xA9charger"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Documents to be uploaded"
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = TELECHARGER, Type = String, Dynamic = True, Default = \"T\xC3\xA9l\xC3\xA9charger", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"T\xC3\xA9l\xC3\xA9charger"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"T\xC3\xA9l\xC3\xA9charger"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Upload"
	#tag EndConstant

	#tag Constant, Name = UPLOADFAILEDPERMISSION, Type = String, Dynamic = True, Default = \"T\xC3\xA9l\xC3\xA9chargement rat\xC3\xA9. Droit d\'acc\xC3\xA8s insuffisants pour ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"T\xC3\xA9l\xC3\xA9chargement rat\xC3\xA9. Droit d\'acc\xC3\xA8s insuffisants pour  "
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"T\xC3\xA9l\xC3\xA9chargement rat\xC3\xA9. Droit d\'acc\xC3\xA8s insuffisants pour "
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Upload failed: Insufficient permissions to save files to "
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
