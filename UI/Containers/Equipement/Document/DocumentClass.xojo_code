#tag Class
Protected Class DocumentClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, DOCUMENTTABLENAME, DOCUMENTPREFIX)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerSuppression(DocumentDB As PostgreSQLDatabase, table_nom As String, value As String) As String
		  // Vérifier si l'enregistrement sélectionné peut être supprimé
		  Dim recordSet as RecordSet
		  Dim strSQL As String = ""
		  
		  Select Case table_nom
		    
		  Case "reppale_descanomalie"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		    
		  End Select
		  
		  recordSet =  DocumentDB .SQLSelect(strSQL)
		  Dim compteur As Integer = recordSet.RecordCount
		  
		  If compteur > 0 Then
		    Return DocumentModule.ENREGNEPEUTPASETREDETRUIT
		  Else
		    Return  "Succes"
		  End If
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		documentation_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		documentation_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		documentation_tri As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		documentation_unite_no As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="documentation_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="documentation_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="documentation_tri"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="documentation_unite_no"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
