#tag WebPage
Begin WebContainer DocumentSideBar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   200
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "931806350"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   360
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   200
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1804472319"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   360
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox DocumentListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   2
      ColumnWidths    =   "6%,94%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   155
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "#DocumentModule.TRI	#DocumentModule.NOMDOCUMENT"
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "0"
      TabOrder        =   -1
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   360
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel DocumentLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      TextAlign       =   0
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   210
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddDocument
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#DocumentModule.HINTADD"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   243
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   688117759
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelDocument
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#DocumentModule.HINTSUPPRIMER"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   279
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   680198143
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TitreTableLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   2
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1056458751"
      TabOrder        =   1
      Text            =   "#DocumentModule.DOCUMENTATION"
      TextAlign       =   0
      Top             =   9
      VerticalCenter  =   0
      Visible         =   True
      Width           =   145
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVVisDocument
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   321
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   883873791
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVUpDocument
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   166
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1484730367
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDownDocument
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   201
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1947779071
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Shown()
		  populateDocument
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ajouterDocuments(documentArray_param() As String)
		  Dim compteur As Integer
		  compteur = DocumentListBox.RowCount  'Pour Debug
		  compteur = DocumentListBox.ListIndex ' Pour Debug
		  compteur = DocumentListBox.LastIndex    'Pour Debug
		  
		  For i As Integer =0 to Ubound(documentArray_param) -1
		    
		    DocumentListBox.insertRow(compteur+1+i, str(compteur+2+i),documentArray_param(i))
		    
		    // Faire pointer le curseur sur la nouvelle ligne
		    DocumentListBox.ListIndex = DocumentListBox.ListIndex + 1
		    documentStruct.no_ligne = DocumentListBox.ListIndex
		    
		    documentStruct.tri =compteur+2+i
		    documentStruct.nom = documentArray_param(i)
		    documentStruct.edit_mode = "Creation"
		    
		    // Création d'un nouvel enregistrement
		    // documentation_tri va prendre la valeur de la ligne actuelle
		    Self.sDocument.documentation_tri = compteur+2+i
		    Self.sDocument.documentation_nom = documentArray_param(i)
		    Self.sDocument.documentation_unite_no = Session.pointeurDocumentSideBar.folderNoUnite
		    
		    documentStruct.id = 0  // indique que c'est un nouvel enregistrement
		    Self.sDocument.documentation_id = 0
		    majDocumentDetail
		  Next i
		  
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  populateDocument
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub appelTelechargement()
		  Dim fileUploader As New FileUploaderContainerControl
		  fileUploader.init(Session.pointeurEquipementContainer)
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementDocument(idDocument_param As Integer)
		  Self.sDocument.listDataByField(Session.bdTechEol, DocumentModule.DOCUMENTTABLENAME,DocumentModule.DOCUMENTPREFIX,  _
		  "documentation_id", str(documentStruct.id), "documentation_tri")
		  generateButtonVisualize
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub descendreDocument()
		  If DocumentListBox.ListIndex < 0 Then Exit Sub
		  If DocumentListBox.ListIndex = DocumentListBox.LastIndex  Then Exit Sub
		  
		  Self.sDocument.documentation_tri  = Self.sDocument.documentation_tri + 2
		  documentStruct.edit_mode = "Creation"
		  documentStruct.no_ligne = documentStruct.no_ligne + 1
		  
		  majDocumentDetail
		  renumerotation
		  populateDocument
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub generateButtonVisualize()
		  If DocumentListBox.ListIndex < 0 Then 
		    IVVisDocument.Visible = False
		    IVVisDocument.Enabled = False
		    Exit Sub
		  End If
		  
		  IVVisDocument.Visible = True
		  IVVisDocument.Enabled = True
		  Dim lienVisualize As String = ""
		  
		  If Session.environnementProp = "Preproduction" Then  // Nous sommes en préProduction
		    lienVisualize = "http://" + HOSTPREPRODUCTION + "/gestequip/documentation/" + Self.sDocument.documentation_unite_no + "/" + Self.sDocument.documentation_nom
		  ElseIf Session.environnementProp = "Test" Then  // Nous sommes en test
		    lienVisualize = "http://" + HOSTTEST + "/techeol/gestequip/documentation/" + Self.sDocument.documentation_unite_no + "/" + Self.sDocument.documentation_nom
		  Else
		    lienVisualize = "http://" + HOSTPROD + "/gestequip/documentation/" + Self.sDocument.documentation_unite_no + "/" + Self.sDocument.documentation_nom
		  End If
		  
		  IVVisDocument.LinkAddTD(lienVisualize)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub majDocumentDetail()
		  
		  If documentStruct.edit_mode = "Creation" Then
		    // Assignation des propriétés aux Contrôles
		    Self.sDocument.assignPropertiesToControls(Self, DocumentModule.DOCUMENTPREFIX)
		  End If
		  
		  Self.sDocument.assignControlToProperties(Self, DocumentModule.DOCUMENTPREFIX)
		  Dim messageErreur  As String = Self.sDocument.writeData(Session.bdTechEol, DocumentModule.DOCUMENTTABLENAME, DocumentModule.DOCUMENTPREFIX)
		  
		  If (messageErreur <> "Succes") Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = DocumentModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit
		  End If
		  
		  
		  documentStruct.id = Self.sDocument.documentation_id
		  documentStruct.nom = Self.sDocument.documentation_nom
		  documentStruct.tri = Self.sDocument.documentation_tri
		  
		  If documentStruct.edit_mode <> "Creation" Then
		    Dim ML6MajModalBox As New MajModal
		    ML6MajModalBox.Show
		  End If
		  
		  documentStruct.edit_mode = "Modification"
		  
		  
		  //startConfirm 
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modDocumentDetail()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub monterDocument()
		  If DocumentListBox.ListIndex < 0 Then Exit Sub
		  If DocumentListBox.ListIndex = 0 Then Exit Sub
		  
		  Self.sDocument.documentation_tri = Self.sDocument.documentation_tri - 2
		  documentStruct.edit_mode = "Creation"
		  documentStruct.no_ligne = documentStruct.no_ligne - 1
		  
		  majDocumentDetail
		  renumerotation
		  populateDocument
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateDocument()
		  
		  If Self.sDocument = Nil Then
		    Self.sDocument = new DocumentClass()
		  End If
		  
		  
		  Dim documentRS As RecordSet
		  documentRS = Self.sDocument.loadDataByFieldCond(Session.bdTechEol,  DocumentModule.DOCUMENTTABLENAME, _
		  "documentation_unite_no", " = ", Session.pointeurDocumentSideBar.folderNoUnite,  " ", "",  " ", "", "documentation_tri", "documentation_id")
		  
		  If documentRS <> Nil Then
		    documentListBox.DeleteAllRows
		    
		    For i As Integer = 1 To documentRS.RecordCount
		      DocumentListBox.AddRow(documentRS.Field("documentation_tri").StringValue, documentRS.Field("documentation_nom").StringValue)
		      
		      // Garder le numéro de ligne
		      DocumentListBox.RowTag(DocumentListBox.LastIndex) = i -1
		      // Garder l'id numéro du Document
		      DocumentListBox.CellTag(DocumentListBox.LastIndex,1) = documentRS.Field("documentation_id").IntegerValue
		      
		      documentRS.MoveNext
		    Next
		    
		    documentRS.Close
		    documentRS = Nil
		    
		    // Positionnement à la ligne gardée en mémoire
		    If DocumentListBox.ListIndex  <> documentStruct.no_ligne And DocumentListBox.RowCount > 0 Then
		      Dim v As Variant = DocumentListBox.LastIndex
		      v = documentStruct.no_ligne
		      If documentStruct.no_ligne > DocumentListBox.LastIndex  Then documentStruct.no_ligne = DocumentListBox.LastIndex
		      DocumentListBox.ListIndex  = documentStruct.no_ligne
		      documentStruct.id = DocumentListBox.CellTag(documentStruct.no_ligne,1)
		    End If
		    
		    generateButtonVisualize
		  End If
		  
		  // Si la liste n'est pas vide, bloquer la possibilité de changement du no. unité
		  Session.pointeurEquipementContainer.equipm_unite_no_TextField.Enabled = True
		  If DocumentListBox.RowCount > 0 Then
		    Session.pointeurEquipementContainer.equipm_unite_no_TextField.Enabled = False
		  End If
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub renumerotation()
		  Dim documentRS As RecordSet
		  Dim compteur As Integer = 1
		  
		  documentRS = Self.sDocument.loadDataByFieldCond(Session.bdTechEol,  DocumentModule.DOCUMENTTABLENAME, _
		  "documentation_unite_no", " = ", Session.pointeurDocumentSideBar.folderNoUnite,  " ", "",  " ", "", "documentation_tri", "documentation_id")
		  
		  While Not documentRS.EOF
		    
		    Self.sDocument.LireDonneesBD(documentRS, DocumentModule.DOCUMENTPREFIX)
		    Self.sDocument.documentation_tri = compteur
		    Dim messageErreur  As String = Self.sDocument.writeData(Session.bdTechEol, DocumentModule.DOCUMENTTABLENAME, DocumentModule.DOCUMENTPREFIX)
		    If (messageErreur <> "Succes") Then
		      Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		      pointeurProbMsgModal.WLAlertLabel.Text = DocumentModule.ERREUR + " " + messageErreur
		      pointeurProbMsgModal.Show
		      Exit
		    End If
		    
		    compteur = compteur+1
		    documentRS.MoveNext
		  Wend
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub resetIV()
		  Self.IVAddDocument.Picture = AjouterModal30x30
		  Self.IVDelDocument.Picture = DeleteModal30x30
		  Self.IVVisDocument.Picture = vizfich30x30
		  Self.IVUpDocument.Picture = MoveUp30x30
		  Self.IVDownDocument.Picture = MoveDown30x30
		  Self.IVVisDocument.Picture = vizfich30x30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub showBoutonAdd()
		  IVAddDocument.Enabled = True
		  IVAddDocument.Visible = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub supDocumentDetail()
		  // Si aucun enreg, on sort
		  If DocumentListBox.RowCount = 0 Then Exit
		  
		  // Vérifier si le répertoire Documentation existe. S'il n'existe pas, le créer
		  Dim documentADetruire As String = DocumentListBox.Cell(DocumentListBox.ListIndex,1)
		  Dim uploadFolder As FolderItem
		  #If DebugBuild Then
		    uploadFolder = App.ExecutableFile.Parent.Parent.Child("Documentation").Child(Session.pointeurDocumentSideBar.folderNoUnite).Child(documentADetruire)
		  #else
		    uploadFolder =App.ExecutableFile.Parent.Child("Documentation").Child(Session.pointeurDocumentSideBar.folderNoUnite).Child(documentADetruire)
		  #Endif
		  
		  If Not uploadFolder.Exists Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = DocumentModule.ERREUR + " " + DocumentModule.DOCUMENTNOTEXISTS
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  //Détruire le document
		  uploadFolder.Delete
		  
		  // L'id  est contenu dans la variable documentStruct.id
		  Dim messageErreur As String = Self.sDocument.supprimerDataByField(Session.bdTechEol, DocumentModule.DOCUMENTTABLENAME, "documentation_id",str(documentStruct.id))
		  
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = DocumentModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		  Else
		    // Renuméroter et Réafficher la liste des projets
		    Self.renumerotation
		    Self.populateDocument
		  End If
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		documentStruct As documentStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		folderNoUnite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		sDocument As DocumentClass
	#tag EndProperty


	#tag Structure, Name = documentStructure, Flags = &h0
		nom As String*100
		  id As Integer
		  no_ligne As Integer
		  tri as Integer
		edit_mode As String*30
	#tag EndStructure


#tag EndWindowCode

#tag Events DocumentListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Rien n'a été sélectionné, réinitialiser les champs
		    //ClearProjetsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    documentStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    documentStruct.id = Me.CellTag(Me.ListIndex,1)
		    changementDocument(Me.CellTag(Me.ListIndex,1))
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddDocument
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVAddDocument.Picture = AjouterModalOrange30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  IVAddDocument.Enabled = False
		  IVAddDocument.Visible = False
		  appelTelechargement
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelDocument
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVDelDocument.Picture = DeleteModalOrange
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  supDocumentDetail
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVVisDocument
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVVisDocument.Picture =vizfichOra30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  // Le bouton est initialisé dans generateButtonVisualize
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVUpDocument
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVUpDocument.Picture = MoveUpOrange30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  monterDocument
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDownDocument
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVDownDocument.Picture = MoveDownOrange30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  descendreDocument
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="folderNoUnite"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
