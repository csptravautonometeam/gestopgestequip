#tag Module
Protected Module DocumentModule
	#tag Constant, Name = CLEETRANGERE, Type = String, Dynamic = True, Default = \"Cl\xC3\xA9 \xC3\xA9trang\xC3\xA8re", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cl\xC3\xA9 \xC3\xA9trang\xC3\xA8re"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cl\xC3\xA9 \xC3\xA9trang\xC3\xA8re"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Foreign key"
	#tag EndConstant

	#tag Constant, Name = DBSCHEMANAME, Type = String, Dynamic = True, Default = \"equip", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DBTABLENAME, Type = String, Dynamic = True, Default = \"equipm_documentation", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DESCRIPTIONANGLAISE, Type = String, Dynamic = True, Default = \"Description anglaise", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Description anglaise"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Description anglaise"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"English description"
	#tag EndConstant

	#tag Constant, Name = DESCRIPTIONFRANCAISE, Type = String, Dynamic = True, Default = \"Description fran\xC3\xA7aise", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Description fran\xC3\xA7aise"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Description fran\xC3\xA7aise"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"French description"
	#tag EndConstant

	#tag Constant, Name = DOCUMENTATION, Type = String, Dynamic = True, Default = \"Documentation", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Documentation"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Documentation"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Documentation"
	#tag EndConstant

	#tag Constant, Name = DOCUMENTNOTEXISTS, Type = String, Dynamic = True, Default = \"Document n\'existe pas", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Document n\'existe pas\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Document n\'existe pas\r\n"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Document does not exist\r\n"
	#tag EndConstant

	#tag Constant, Name = DOCUMENTPREFIX, Type = String, Dynamic = True, Default = \"documentation", Scope = Public
	#tag EndConstant

	#tag Constant, Name = DOCUMENTTABLENAME, Type = String, Dynamic = True, Default = \"equipm_documentation", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ENREGNEPEUTPASETREDETRUIT, Type = String, Dynamic = True, Default = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Record cannot be deleted. It is still part of a database record."
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = FORMULAIRE, Type = String, Dynamic = True, Default = \"Formulaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Formulaire"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Formulaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Form"
	#tag EndConstant

	#tag Constant, Name = HINTADD, Type = String, Dynamic = True, Default = \"Ajouter", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ajouter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ajouter"
	#tag EndConstant

	#tag Constant, Name = HINTLOCKUNLOCK, Type = String, Dynamic = True, Default = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lock/Unlock record"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
	#tag EndConstant

	#tag Constant, Name = HINTSUPPRIMER, Type = String, Dynamic = True, Default = \"Supprimer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
	#tag EndConstant

	#tag Constant, Name = HINTUPDATEDETAILS, Type = String, Dynamic = True, Default = \"Enregistrer les modifications.", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrer les modifications."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Save modifications."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrer les modifications."
	#tag EndConstant

	#tag Constant, Name = NOMDOCUMENT, Type = String, Dynamic = True, Default = \"Nom du document", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nom du document"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Document name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom du document"
	#tag EndConstant

	#tag Constant, Name = NOUVELLECLE, Type = String, Dynamic = True, Default = \"Nouvelle cl\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nouvelle cl\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"New key"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nouvelle cl\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = NOUVELLEDESCRIPTIONANGLAISE, Type = String, Dynamic = True, Default = \"Nouvelle description anglaise", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nouvelle description anglaise"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"New english description"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nouvelle description anglaise"
	#tag EndConstant

	#tag Constant, Name = NOUVELLEDESCRIPTIONFRANCAISE, Type = String, Dynamic = True, Default = \"Nouvelle description fran\xC3\xA7aise", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nouvelle description fran\xC3\xA7aise"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"New french description"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nouvelle description fran\xC3\xA7aise"
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Statut", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = TABLETRANGERE, Type = String, Dynamic = True, Default = \"Table \xC3\xA9trang\xC3\xA8re", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Table \xC3\xA9trang\xC3\xA8re"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Table \xC3\xA9trang\xC3\xA8re"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Foreign table"
	#tag EndConstant

	#tag Constant, Name = TRI, Type = String, Dynamic = True, Default = \"Tri", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Tri"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Sort"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Tri"
	#tag EndConstant

	#tag Constant, Name = ZONEDOITETRENUMERIQUE, Type = String, Dynamic = True, Default = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This field must be a numeric value"
	#tag EndConstant

	#tag Constant, Name = ZONESOBLIGATOIRES, Type = String, Dynamic = True, Default = \"Required field(s)", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required field(s)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Zone(s) obligatoire(s)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required field(s)"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
