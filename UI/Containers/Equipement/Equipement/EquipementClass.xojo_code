#tag Class
Protected Class EquipementClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h1000
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, EQUIPEMENTTABLENAME ,equipementprefix)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function genClauseWherePremier(listeSelection_param As Dictionary) As String
		  
		  Dim listeSelection As String = ""
		  Dim keys(), k, v as variant
		  
		  keys = listeSelection_param.keys()
		  for each k in keys
		    Select Case k
		    Case "TypeEquipement"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and equipm_type IN('" + v + "')  "
		    End Select
		  Next
		  
		  Return listeSelection
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function genClauseWhereSecond(listeSelection_param As Dictionary) As String
		  
		  Dim listeSelection As String = ""
		  Dim keys(), k, v as variant
		  
		  keys = listeSelection_param.keys()
		  for each k in keys
		    Select Case k
		    Case "Revision"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and inspveh_revision IN(" + v + ")  "
		    Case "TypeEquipement"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and equipm_type IN('" + v + "')  "
		    End Select
		  Next
		  
		  Return listeSelection
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataEquipInspecSequence(dB As PostgreSQLDatabase, listeSelection_param As Dictionary) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  // Génération de la clause Where
		  Dim clauseWhere As  String = genClauseWhereSecond(listeSelection_param)
		  
		  If clauseWhere <> "" Then
		    // Ajout du Where et enlever le premier And
		    clauseWhere = " Where " + mid(clauseWhere,4)
		  End If
		  
		  strSQL = "SELECT equipm_id, equipm_unite_no,  equipm_type, equipm_statut, "+ _
		  "(CASE WHEN equipm_marque Is Null Then '' Else equipm_marque || ' ' END || " + _
		  " CASE WHEN equipm_modele Is Null Then ' ' Else equipm_modele || ' ' END ||" + _
		  " CASE WHEN equipm_annee    Is Null Then ' ' Else equipm_annee   || ' ' END ||" + _
		  " CASE WHEN equipm_couleur Is Null Then ' ' Else equipm_couleur || ' ' END ||" + _
		  " CASE WHEN equipm_moteur Is Null Then ' ' Else equipm_moteur  || ' ' END) " + _
		  " As equipm_description, " + _
		  "(select parametre_desc_fr  from parametre where parametre_nom = 'typeequipement'   And equipm_type = parametre_cle Limit 1)  as equipm_type_description,  " + _
		  "CASE WHEN  char_length(equipm_localisation) <= 5 Then " + _
		  "(select parametre_desc_fr  from parametre where parametre_nom = 'localisation'   And equipm_localisation = parametre_cle Limit 1) " + _
		  " Else (select projet_no || ' ' || projet_titre  from projet where equipm_localisation = projet_no Limit 1)  END As equipm_localisation_description, " + _
		  "(select employe_nom || ' ' || employe_prenom  from employe where employe_nas = inspveh_inspecteur_nas Limit 1)  as inspveh_inspecteur_nas_nom,  " + _
		  "(CASE WHEN inspveh_revision = '2' Then '' ELSE '" + EquipementModule.REVISE + "'  END)  As inspveh_revision_desc, " +_
		  " equipm_km_heures, inspveh_id, inspveh_kilometrage, inspveh_inspecteur_nas, inspveh_date_inspection, inspveh_dommages_connus, inspveh_revision " + _
		  "FROM equipm  inner join inspection_vehicule on equipm_unite_no = inspveh_no_unite " + _
		  clauseWhere + _
		  "order by equipm_unite_no , inspveh_kilometrage desc " 
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataEquipSequence(dB As PostgreSQLDatabase, listeSelection_param As Dictionary) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  // Génération de la clause Where
		  Dim clauseWhere As  String = genClauseWherePremier(listeSelection_param)
		  
		  If clauseWhere <> "" Then
		    // Ajout du Where et enlever le premier And
		    clauseWhere = " Where " + mid(clauseWhere,4)
		  End If
		  
		  strSQL = "SELECT equipm_id, equipm_unite_no,  equipm_type, equipm_statut, "+ _
		  "(CASE WHEN equipm_marque Is Null Then '' Else equipm_marque || ' ' END || " + _
		  " CASE WHEN equipm_modele Is Null Then ' ' Else equipm_modele || ' ' END ||" + _
		  " CASE WHEN equipm_annee    Is Null Then ' ' Else equipm_annee   || ' ' END ||" + _
		  " CASE WHEN equipm_couleur Is Null Then ' ' Else equipm_couleur || ' ' END ||" + _
		  " CASE WHEN equipm_moteur Is Null Then ' ' Else equipm_moteur  || ' ' END) " + _
		  " As equipm_description, " + _
		  "(select parametre_desc_fr  from parametre where parametre_nom = 'typeequipement'   And equipm_type = parametre_cle Limit 1)  as equipm_type_description,  " + _
		  "CASE WHEN  char_length(equipm_localisation) <= 5 Then " + _
		  "(select parametre_desc_fr  from parametre where parametre_nom = 'localisation'   And equipm_localisation = parametre_cle Limit 1) " + _
		  " Else (select projet_no || ' ' || projet_titre  from projet where equipm_localisation = projet_no Limit 1)  END As equipm_localisation_description, " + _
		  " equipm_localisation_date, equipm_locat_date_fin, equipm_inspect_date_saaq_exp, equipm_km_heures, equipm_locat_km_permis, equipm_certcalib_date, equipm_certcalib_date_due " + _
		  "FROM equipm  " + _
		  clauseWhere + _
		  "order by equipm_unite_no  " 
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerSuppression(dB As PostgreSQLDatabase, equipement_id As String) As String
		  // Vérifier si l'enregistrement sélectionné peut être supprimé
		  Dim recordSet as RecordSet
		  Dim strSQL As String = ""
		  
		  //strSQL = "SELECT * FROM photo WHERE  photo_index = '" + projet_id + "'" 
		  //recordSet =  dB .SQLSelect(strSQL)
		  //compteur = recordSet.RecordCount
		  //recordSet.Close
		  recordSet = Nil
		  //
		  //If compteur > 0 Then 
		  //Return Strings.ENREGNONDETRUIT
		  //End If
		  
		  Return "Succes"
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(equipDB As PostgreSQLDatabase, equipm_unite_no_param As WebTextField) As String
		  
		  // Vérification des zones obligatoires
		  If equipm_unite_no_param .Text = ""   Then
		    equipm_unite_no_param .Style = TextFieldErrorStyle
		    Return EquipementModule.ZONESOBLIGATOIRES
		  End If
		  
		  // Vérifier si le nuo. unité commence par T-
		  If mid(equipm_unite_no_param .Text,1,2) <> "T-"   Then
		    equipm_unite_no_param .Style = TextFieldErrorStyle
		    Return EquipementModule.UNITENOCOMMENCEPART
		  End If
		  
		  // Vérifier si la clé entrée (equipm_unite_no) est différente et existe déjà dans la BD
		  If equipm_unite_no_param .Text <> Session.pointeurEquipementSideBarContainer.sEquipement.equipm_unite_no Then
		    // Mode Modification, Lire les données
		    Dim equipementRS As RecordSet = Session.pointeurEquipementSideBarContainer.sEquipement.loadDataByField(Session.bdTechEol, EQUIPEMENTTABLENAME, _
		    "equipm_unite_no", equipm_unite_no_param.Text, "equipm_unite_no")
		    If equipementRS = Nil Then GoTo Suivant
		    
		    Dim compteur As Integer = equipementRS.RecordCount
		    equipementRS.Close
		    equipementRS = Nil
		    If compteur > 0 Then Return EquipementModule.NOUNITEEXISTEDEJA
		  End If
		  
		  Suivant:
		  
		  Return "Succes"
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		equipm_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_localisation As String
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_localisation_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_marque As String
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_modele As String
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_mot_cle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_statut As String
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_type As String
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_unite_no As String
	#tag EndProperty

	#tag Property, Flags = &h0
		equipm_valeur As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="equipm_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_localisation"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_localisation_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_marque"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_modele"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_mot_cle"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_statut"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_type"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_unite_no"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="equipm_valeur"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
