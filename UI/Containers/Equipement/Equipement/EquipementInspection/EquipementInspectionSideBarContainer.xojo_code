#tag WebPage
Begin WebContainer EquipementInspectionSideBarContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   550
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1100
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle EquipementInspectionRectangle
      Cursor          =   0
      Enabled         =   True
      Height          =   550
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1622472703"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox EquipementInspectionSideBarListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   9
      ColumnWidths    =   "5%,15%,8%,18%,7%,7%,12%,21%,7%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   469
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "#EquipementModule.UNITENO	Description	#EquipementModule.TYPEEQUIPEMENT	#EquipementModule.LOCALISATION	#EquipementModule.DATEINSPECTION	#EquipementModule.KMHEURES	#EquipementModule.INSPECTEUR	#EquipementModule.DOMMAGESCONNUS	#EquipementModule.REVISION"
      Left            =   5
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "0"
      TabOrder        =   -1
      Top             =   80
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1090
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin CritereContainerControl EquipementInspectionSideBarContainerCritereContainerControl
      Cursor          =   0
      Enabled         =   True
      Height          =   80
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   0
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   894
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModEquipementInspection
      AlignHorizontal =   0
      AlignVertical   =   3
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1020
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1064464383
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelEquipementInspection
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   906
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   680198143
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddEquipementInspection
      AlignHorizontal =   2
      AlignVertical   =   0
      Cursor          =   1
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   940
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   688117759
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVRafEquipementInspection
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1055
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   644532223
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVVisEquipementInspection
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   985
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   883873791
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Shown()
		  populateEquipementInspection
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ajouterEquipementInspection()
		  Dim compteur As Integer
		  compteur = EquipementInspectionSideBarListBox.RowCount  'Pour Debug
		  compteur = EquipementInspectionSideBarListBox.LastIndex    'Pour Debug
		  compteur = EquipementInspectionSideBarListBox.ListIndex ' Pour Debug
		  
		  //EquipementInspectionSideBarListBox.insertRow(compteur+1, "T-00-0000", "toeo", Self.sEquipementInspection.equipm_localisation_date, , "A")
		  // Faire pointer le curseur sur le nouveau projet
		  //EquipementInspectionSideBarListBox.ListIndex = EquipementInspectionSideBarListBox.ListIndex + 1
		  //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.no_ligne = EquipementInspectionSideBarListBox.ListIndex
		  //EquipementInspectionSideBarListBox.ListIndex = 0
		  //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.no_ligne = EquipementInspectionSideBarListBox.ListIndex
		  
		  //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.cle = "T-"
		  //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.type = "toeo"
		  //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.statut = "A"
		  //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.edit_mode = "Creation"
		  
		  // Création d'un nouvel enregistrement d'équipement
		  //Dim indexCritere As Integer = EquipementInspectionSideBarContainerCritereContainerControl.TypeEquipementPopupMenu.ListIndex
		  //Dim classCritere As String = "toeo"
		  //If indexCritere <= 0 Then
		  //Self.sEquipementInspection = new AllEquipmentToolClass()
		  //Else
		  //classCritere = EquipementInspectionSideBarContainerCritereContainerControl.TypeEquipementPopupMenu.RowTag(indexCritere)
		  //Self.sEquipementInspection = selectEquipementClass(classCritere)
		  //End If
		  //Self.sEquipementInspection.equipm_unite_no = "T-"
		  //Self.sEquipementInspection.equipm_type = classCritere
		  //Dim dateTrav As Date = new Date()
		  //Self.sEquipementInspection.equipm_localisation_date = str(dateTrav.Year) + "-" +str(dateTrav.Month) + "-" + str(dateTrav.Day)
		  //Self.sEquipementInspection.equipm_statut = "A"
		  
		  //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.id = 0  // indique que c'est un nouvel enregistrement
		  //Self.sEquipementInspection.equipm_id = 0
		  //Dim ML6EquipementWebDialog As EquipementWebDialog = New EquipementWebDialog
		  //ML6EquipementWebDialog.EquipementContainerDialog.majEquipementDetail
		  //ML6EquipementWebDialog = Nil
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  //populateEquipementInspection
		  
		  // Se mettre en mode lock
		  //implementLockInspection
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub changementEquipementInspection(idEquipementInspection_param As Integer)
		  // Si la valeur de idEquipement est 0, c'est un nouvel enregistrement, donc pas de lecture de la BD
		  If idEquipementInspection_param  = 0 Then
		    //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.edit_mode = "Creation"
		  Else
		    //Self.sEquipementInspection = Nil
		    // Passer le type d'équipement en paramètre
		    //Self.sEquipementInspection = selectEquipementinspectionClass(EquipementInspectionSideBarListBox.CellTag(EquipementInspectionSideBarListBox.ListIndex,2))
		    Dim stest As EquipementClass = Self.sEquipementInspection
		    // Lecture
		    Self.sEquipementInspection.listDataByField(Session.bdTechEol, EQUIPEMENTTABLENAME, EquipementModule.equipementprefix,  _
		    "equipm_id", str(Session.pointeurEquipementInspectionSideBarContainer.equipementInspectionStruct.id), "equipm_id")
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  //Super.Constructor
		  
		  // Écraser les paramètres par défaut de la classe
		  
		  //Me.Width = 1200
		  //Me.LockLeft = True
		  //Me.LockRight = True
		  //Me.LockTop = True
		  //Me.LockBottom = True
		  //Me.Top = 0
		  //Me.Left = 0 
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub generateButtonVisualize()
		  If EquipementInspectionSideBarListBox.ListIndex = -1  Then
		    IVVisEquipementInspection.Visible = False
		    IVVisEquipementInspection.Enabled = False
		    Exit Sub
		  End If
		  
		  // Vérifier s'il y a des photos à voir
		  IVVisEquipementInspection.Visible = False
		  IVVisEquipementInspection.Enabled = False
		  //IVVisEquipementInspection.Visible = True
		  //IVVisEquipementInspection.Enabled = True
		  Dim lienVisualize As String = ""
		  
		  //If Session.environnementProp = "Preproduction" Then  // Nous sommes en préProduction
		  //lienVisualize = "http://" + HOSTPREPRODUCTION + "/gestequip/Inspection/" + Self.sEquipementInspection.EquipementInspectionation_unite_no + "/" + Self.sEquipementInspection.EquipementInspectionation_nom
		  //ElseIf Session.environnementProp = "Test" Then  // Nous sommes en test
		  //lienVisualize = "http://" + HOSTTEST + "/techeol/gestequip/Inspection/" + Self.sEquipementInspection.EquipementInspectionation_unite_no + "/" + Self.sEquipementInspection.EquipementInspectionation_nom
		  //Else
		  //lienVisualize = "http://" + HOSTPROD + "/gestequip/Inspection/" + Self.sEquipementInspection.EquipementInspectionation_unite_no + "/" + Self.sEquipementInspection.EquipementInspectionation_nom
		  //End If
		  
		  //IVVisEquipement.LinkAddTD(lienVisualize)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub implementLockInspection()
		  Dim message As String = ""
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim equip_recherche As String = EquipementModule.DBSCHEMANAME + "." + EquipementModule.DBTABLENAME + "." + str(Self.sEquipementInspection.equipm_id)
		  message = Self.sEquipementInspection.check_If_Locked(Session.bdTechEol, equip_recherche)
		  If  message <> "Locked" And message <> "Unlocked" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If Message = "Locked" Then
		    message = EquipementModule.ENREGVERROUILLEPAR + Self.sEquipementInspection.lockedBy
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  //Activer le mode Verrouillage
		  Self.sEquipementInspection.lockEditMode = True
		  
		  //Creer les valeurs de verrouillage record ID
		  Self.sEquipementInspection.recordID = EquipementModule.DBSCHEMANAME + "." +EquipementModule.DBTABLENAME+ "." + str(Self.sEquipementInspection.equipm_id)
		  message = Self.sEquipementInspection.lockRow(Session.bdTechEol) 
		  If message <> "Succes" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  //Dim ML6EquipementWebDialog As EquipementWebDialog = New EquipementWebDialog
		  // Appeler la modification
		  //ML6EquipementWebDialog.EquipementContainerDialog.lisEquipementInspectionInspectionDetail
		  //ML6EquipementWebDialog.Show
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub loadImageLogo(image as WebImageView)
		  
		  //image.Picture = CartierRepPalesLogo350x100
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub modifierEquipementInspection()
		  //Déclenchement du mode édition
		  If EquipementInspectionSideBarListBox.ListIndex <> -1 And EquipementInspectionSideBarListBox.RowCount > 0 Then
		    Dim equipementInspectionVehiculeWebDialog As InspectionVehiculeWebDialog = new InspectionVehiculeWebDialog
		    Session.pointeurInspectionVehiculeWebDialog = equipementInspectionVehiculeWebDialog
		    Dim valeur_unite_no As String = EquipementInspectionSideBarListBox.CellTag(EquipementInspectionSideBarListBox.ListIndex,3) 
		    Session.no_unite = EquipementInspectionSideBarListBox.CellTag(EquipementInspectionSideBarListBox.ListIndex,3) 
		    Session.pointeurInspectionVehiculeWebDialog.InspectionVehiculeSideBarDialog.populateInspectionVehicule
		    Session.pointeurInspectionVehiculeWebDialog.Show
		    
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub modifierValeurRevision(inspveh_id_param As Integer)
		  
		  Dim sInspectionVehicule As  InspectionVehiculeClass = new InspectionVehiculeClass()
		  
		  // Mode Modification, Lire les données
		  Dim inspectionVehiculeRS As RecordSet = sInspectionVehicule.loadDataByField(Session.bdTechEol, InspectionVehiculeModule.INSPECTIONVEHICULETABLENAME, _
		  "inspveh_id", str(inspveh_id_param), "inspveh_id")
		  
		  If inspectionVehiculeRS <> Nil Then sInspectionVehicule.LireDonneesBD(inspectionVehiculeRS, InspectionVehiculeModule.INSPECTIONVEHICULEPREFIX)
		  
		  inspectionVehiculeRS.Close
		  inspectionVehiculeRS = Nil
		  
		  If sInspectionVehicule.inspveh_revision = "2" Then
		    sInspectionVehicule.inspveh_revision = "1"
		  Else
		    sInspectionVehicule.inspveh_revision = "2"
		  End
		  
		  Dim messageErreur  As String = sInspectionVehicule.writeData(Session.bdTechEol, InspectionVehiculeModule.INSPECTIONVEHICULETABLENAME, InspectionVehiculeModule.INSPECTIONVEHICULEPREFIX)
		  
		  sInspectionVehicule = Nil
		  
		  If (messageErreur <> "Succes") Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = InspectionVehiculeModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit
		  End If
		  
		  Self.populateEquipementInspection(inspveh_id_param)
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateEquipementInspection(Optional inspveh_id_param as Integer)
		  If Self.sEquipementInspection = Nil Then
		    Self.sEquipementInspection = new EquipementClass()
		  End If
		  
		  Dim listeSelection As Dictionary = EquipementInspectionSideBarContainerCritereContainerControl.chargementListeSelection
		  
		  Dim equipementInspectionRS As RecordSet
		  equipementInspectionRS = Self.sEquipementInspection.loadDataEquipInspecSequence(Session.bdTechEol, listeSelection)
		  
		  EquipementInspectionSideBarListBox.DeleteAllRows
		  
		  // S'il n'y a rien, on sort
		  If equipementInspectionRS = Nil Then Exit Sub
		  
		  For i As Integer = 1 To equipementInspectionRS.RecordCount
		    EquipementInspectionSideBarListBox.AddRow(equipementInspectionRS.Field("equipm_unite_no").StringValue, equipementInspectionRS.Field("equipm_description").StringValue, _
		    equipementInspectionRS.Field("equipm_type_description").StringValue, equipementInspectionRS.Field("equipm_localisation_description").StringValue, _
		    equipementInspectionRS.Field("inspveh_date_inspection").StringValue, equipementInspectionRS.Field("inspveh_kilometrage").StringValue, _
		    equipementInspectionRS.Field("inspveh_inspecteur_nas_nom").StringValue, equipementInspectionRS.Field("inspveh_dommages_connus").StringValue, _
		    equipementInspectionRS.Field("inspveh_revision_desc").StringValue)
		    
		    If equipementInspectionRS.Field("equipm_statut").StringValue <> "A" Then
		      For j As Integer = 0 To 4
		        EquipementInspectionSideBarListBox.CellStyle(i-1, j) = BGGray200
		      Next
		    End If
		    
		    // Garder le numéro de ligne
		    EquipementInspectionSideBarListBox.RowTag(EquipementInspectionSideBarListBox.LastIndex) = i -1
		    // Garder l'id numéro du equipement
		    EquipementInspectionSideBarListBox.CellTag(EquipementInspectionSideBarListBox.LastIndex,1) = equipementInspectionRS.Field("inspveh_id").IntegerValue
		    // Garder le type d'équipement
		    EquipementInspectionSideBarListBox.CellTag(EquipementInspectionSideBarListBox.LastIndex,2) = equipementInspectionRS.Field("equipm_type").StringValue
		    // Garder l'id de l'inspection
		    EquipementInspectionSideBarListBox.CellTag(EquipementInspectionSideBarListBox.LastIndex,3) = equipementInspectionRS.Field("equipm_unite_no").StringValue
		    
		    equipementInspectionRS.MoveNext
		  Next
		  
		  equipementInspectionRS.Close
		  equipementInspectionRS = Nil
		  
		  Self.generateButtonVisualize
		  
		  // Positionnement par rapport à un numéro d'inspection
		  If inspveh_id_param <> 0 Then
		    For indexListBox As Integer = 0 To EquipementInspectionSideBarListBox.RowCount - 1
		      If EquipementInspectionSideBarListBox.CellTag(indexListBox,1) = inspveh_id_param Then
		        Session.pointeurEquipementInspectionSideBarContainer.equipementInspectionStruct.no_ligne = indexListBox
		        EquipementInspectionSideBarListBox.ListIndex  = indexListBox
		        Session.pointeurEquipementInspectionSideBarContainer.equipementInspectionStruct.id = EquipementInspectionSideBarListBox.CellTag(Session.pointeurEquipementInspectionSideBarContainer.equipementInspectionStruct.no_ligne,1)
		        Exit Sub
		      End If
		    Next
		    Exit Sub
		  End If
		  
		  // Positionnement à la ligne gardée en mémoire si la liste n'est pas vide
		  //If EquipementInspectionSideBarListBox.ListIndex  <> Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.no_ligne And EquipementInspectionSideBarListBox.RowCount > 0 Then
		  //Dim v As Variant = EquipementInspectionSideBarListBox.LastIndex
		  //v = Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.no_ligne
		  //If Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.no_ligne > EquipementInspectionSideBarListBox.LastIndex  Then Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.no_ligne = EquipementInspectionSideBarListBox.LastIndex
		  //EquipementInspectionSideBarListBox.ListIndex  = Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.no_ligne
		  //Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.id = EquipementInspectionSideBarListBox.CellTag(Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.no_ligne,1)
		  //End If
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub resetIV()
		  IVModEquipementInspection.Picture = ModModal30x30
		  IVAddEquipementInspection.Picture = AjouterModal30x30
		  IVRafEquipementInspection.Picture = RefreshMenu30x30
		  IVDelEquipementInspection.Picture = DeleteModal30x30
		  IVVisEquipementInspection.Picture = vizfich30x30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function selectEquipementInspectionClass(typeEquipement As String) As EquipementClass
		  Dim equipementClass As EquipementClass
		  
		  //Select Case typeEquipement 
		  //
		  //Case "voit"
		  //Dim voitureClassInst As VoitureClass = new VoitureClass
		  //equipementClass = voitureClassInst
		  //
		  //Case "cami"
		  //Dim camionClassInst As CamionClass = new CamionClass
		  //equipementClass = camionClassInst
		  //
		  //Case "civi", "tsgs"
		  //Dim civCoulisseauClassInst As CivCoulisseauClass = new CivCoulisseauClass
		  //equipementClass = civCoulisseauClassInst
		  //
		  //Case "detc", "durb", "durs", "erad", "mumg", "piam", "pmph", "thha", "thin", "towr", "maal"
		  //Dim eqOutilSelClassInst As EqOutilSelClass = new EqOutilSelClass
		  //equipementClass = eqOutilSelClassInst
		  //
		  //Case "kits", "pltf"
		  //Dim kitPltfClassInst As KitPltfClass = new KitPltfClass
		  //equipementClass = kitPltfClassInst
		  //
		  //Case "remo", "remf"
		  //Dim remClassInst As RemClass = new RemClass
		  //equipementClass = remClassInst
		  //
		  //Case "damu"
		  //Dim damClassInst As DamClass = new DamClass
		  //equipementClass = damClassInst
		  //
		  //Case "pelj", "defb", "extc"
		  //Dim peDeExClassInst As PeDeExClass = new PeDeExClass
		  //equipementClass = peDeExClassInst
		  //
		  //Case "bore", "trai"
		  //Dim borTraiClassInst As BorTraiClass = new BorTraiClass
		  //equipementClass = borTraiClassInst
		  //
		  //Case "gene"
		  //Dim genClassInst As GenClass = new GenClass
		  //equipementClass = genClassInst
		  //
		  //Case "motn"
		  //Dim motClassInst As MotClass = new MotClass
		  //equipementClass = motClassInst
		  //
		  //Case "trec"
		  //Dim tourClassInst As TourClass = new TourClass
		  //equipementClass = tourClassInst
		  //
		  //Case "vrad", "tens"
		  //Dim vradTensClassInst As VradTensClass = new VradTensClass
		  //equipementClass = vradTensClassInst
		  //
		  //Case "toeo"
		  //Dim allEquipmentToolClassInst As AllEquipmentToolClass = new AllEquipmentToolClass
		  //equipementClass = allEquipmentToolClassInst
		  //
		  //Case Else // par défaut
		  //Dim equiClass As EquipementClass = new EquipementClass
		  //equipementClass = equiClass
		  //
		  //End Select
		  
		  Return equipementClass
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub supEquipementInspectionDetail()
		  
		  // Vérifier si un enregistrement peut être supprimé
		  Dim messageErreur as String = ""
		  messageErreur =  Self.sEquipementInspection.validerSuppression(Session.bdTechEol, Self.sEquipementInspection.equipm_unite_no)
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = EquipementModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim equip_recherche As String = EquipementModule.DBSCHEMANAME + "." + EquipementModule.DBTABLENAME + "." + str(Self.sEquipementInspection.equipm_id)
		  messageErreur = Self.sEquipementInspection.check_If_Locked(Session.bdTechEol, equip_recherche)
		  If  messageErreur <> "Locked" And messageErreur <> "Unlocked" Then
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If messageErreur = "Locked" Then
		    messageErreur = EquipementModule.ENREGVERROUILLEPAR + Self.sEquipementInspection.lockedBy
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  
		  // L'id  est contenu dans la variable Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.id
		  //messageErreur = Self.sEquipementInspection.supprimerDataByField(Session.bdTechEol, EquipementModule.EQUIPEMENTTABLENAME, "equipm_id",str(Session.pointeurEquipementInspectionSideBarContainer.equipementStruct.id))
		  
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = EquipementModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		  Else
		    // Renuméroter et Réafficher la liste des projets
		    Self.populateEquipementInspection
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub unlockInspection()
		  Dim lockedRowIDBis As integer = Session.sauvegardeLockedRowID
		  Dim message As String = Self.sEquipementInspection.unLockRow(Session.bdTechEol)
		  If  message <> "Succes" Then
		    afficherMessage(message)
		  End If
		  
		  Self.sEquipementInspection.lockEditMode = False
		  Self.Close
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		equipementInspectionStruct As equipementinspectionStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		sEquipementInspection As EquipementClass
	#tag EndProperty


	#tag Structure, Name = equipementInspectionStructure, Flags = &h0
		cle As String*30
		  id As Integer
		  no_ligne As Integer
		  edit_mode As String*30
		  table_nom As String*30
		  statut As String*1
		type As String*4
	#tag EndStructure


#tag EndWindowCode

#tag Events EquipementInspectionSideBarListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Ancun equipement n'a été sélectionné, réinitialiser les champs
		    //ClearEquipementsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    Session.pointeurEquipementInspectionSideBarContainer.equipementInspectionStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    Session.pointeurEquipementInspectionSideBarContainer.equipementInspectionStruct.id = Me.CellTag(Me.ListIndex,1)
		    Dim valeur As Integer = Me.CellTag(Me.ListIndex,1)
		    changementEquipementInspection(Me.CellTag(Me.ListIndex,1))
		    generateButtonVisualize
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub DoubleClick(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  resetIV
		  modifierEquipementInspection
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModEquipementInspection
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Me.Picture = ModModalOrange30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If EquipementInspectionSideBarListBox.ListIndex <> -1 And EquipementInspectionSideBarListBox.RowCount > 0 Then
		    resetIV
		    modifierValeurRevision(EquipementInspectionSideBarListBox.CellTag(EquipementInspectionSideBarListBox.ListIndex,1))
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelEquipementInspection
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Me.Picture = DeleteModalOrange
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  supEquipementInspectionDetail
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddEquipementInspection
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Me.Picture = AjouterModalOrange30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ajouterEquipementInspection
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVRafEquipementInspection
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Me.Picture = RefreshMenuOra30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  resetIV
		  populateEquipementInspection
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVVisEquipementInspection
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVVisEquipementInspection.Picture =vizfichOra30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  // Le bouton est initialisé dans generateButtonVisualize
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
