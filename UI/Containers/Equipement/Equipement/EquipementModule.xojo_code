#tag Module
Protected Module EquipementModule
	#tag Constant, Name = ANNEE, Type = String, Dynamic = True, Default = \"Ann\xC3\xA9e", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ann\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Year"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ann\xC3\xA9e"
	#tag EndConstant

	#tag Constant, Name = ASSISTANCEUSURE, Type = String, Dynamic = True, Default = \"Assist. usure", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Assist. usure"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Wear assist."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Assist. usure"
	#tag EndConstant

	#tag Constant, Name = BAILEXP, Type = String, Dynamic = True, Default = \"Bail expiration", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Bail expiration"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Rent expiration"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Bail expiration"
	#tag EndConstant

	#tag Constant, Name = CERTIFCALIB, Type = String, Dynamic = True, Default = \"Certitification/Calibration", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Certitification/Calibration"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Certitification/Calibration"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Certitification/Calibration"
	#tag EndConstant

	#tag Constant, Name = CERTIFCALIBDATE, Type = String, Dynamic = True, Default = \"Cert/Calib date", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cert/Calib date"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cert/Calib date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cert/Calib date"
	#tag EndConstant

	#tag Constant, Name = CERTIFCALIBDATEDUE, Type = String, Dynamic = True, Default = \"Cert/Calib date due", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cert/Calib date due\r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cert/Calib due date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cert/Calib date due\r"
	#tag EndConstant

	#tag Constant, Name = CHANGHUILEKMH, Type = String, Dynamic = True, Default = \"Changem. huile Km/heures", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Changem. huile Km/heures"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Oil change Km/hours"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Changem. huile Km/heures"
	#tag EndConstant

	#tag Constant, Name = COULEUR, Type = String, Dynamic = True, Default = \"Couleur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Couleur"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Couleur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Color"
	#tag EndConstant

	#tag Constant, Name = DATECUE, Type = String, Dynamic = True, Default = \"AAAA-MM-JJ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"AAAA-MM-JJ\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"AAAA-MM-JJ"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"YYYY-MM-DD"
	#tag EndConstant

	#tag Constant, Name = DATEDEFIN, Type = String, Dynamic = True, Default = \"Date de fin", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de fin"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de fin"
	#tag EndConstant

	#tag Constant, Name = DATEDUE, Type = String, Dynamic = True, Default = \"Date due", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date due"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Due date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date due"
	#tag EndConstant

	#tag Constant, Name = DATEEXPSAAQ, Type = String, Dynamic = True, Default = \"SAAQ Date exp", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"SAAQ Date exp"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"SAAQ Exp date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"SAAQ Date exp"
	#tag EndConstant

	#tag Constant, Name = DATEINSPECTION, Type = String, Dynamic = True, Default = \"Date d\'inspection", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date d\'inspection"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Inspection date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date d\'inspection"
	#tag EndConstant

	#tag Constant, Name = DATEINSPSAAQ, Type = String, Dynamic = True, Default = \"SAAQ Date insp", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"SAAQ Date insp"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"SAAQ Insp date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"SAAQ Date insp"
	#tag EndConstant

	#tag Constant, Name = DBSCHEMANAME, Type = String, Dynamic = True, Default = \"equip", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DBTABLENAME, Type = String, Dynamic = True, Default = \"equipm", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DETAILEQUIPEMENT, Type = String, Dynamic = True, Default = \"D\xC3\xA9tail \xC3\xA9quipement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"D\xC3\xA9tail \xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment detail"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9tail \xC3\xA9quipement"
	#tag EndConstant

	#tag Constant, Name = DOMMAGE, Type = String, Dynamic = True, Default = \"Dommage", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Dommage"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Damage"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Dommage"
	#tag EndConstant

	#tag Constant, Name = DOMMAGEDATE, Type = String, Dynamic = True, Default = \"Date dommage", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date dommage"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Damage date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date dommage"
	#tag EndConstant

	#tag Constant, Name = DOMMAGESCONNUS, Type = String, Dynamic = True, Default = \"Dommages connus", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Dommages connus"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Known damages"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Dommages connus"
	#tag EndConstant

	#tag Constant, Name = DOMMAGESTATUT, Type = String, Dynamic = True, Default = \"Statut dommage", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut dommage"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Damage status"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut dommage"
	#tag EndConstant

	#tag Constant, Name = EMPLACEMENT, Type = String, Dynamic = True, Default = \"Emplacement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Emplacement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Location"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Emplacement"
	#tag EndConstant

	#tag Constant, Name = ENREGNEPEUTPASETREDETRUIT, Type = String, Dynamic = True, Default = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Record cannot be deleted. It is still part of a database record."
	#tag EndConstant

	#tag Constant, Name = ENREGVERROUILLEPAR, Type = String, Dynamic = True, Default = \"Enregistrement est verrouill\xC3\xA9 par l\'utilisateur ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrement est verrouill\xC3\xA9 par l\'utilisateur \r"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrement est verrouill\xC3\xA9 par l\'utilisateur \r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Record locked by user "
	#tag EndConstant

	#tag Constant, Name = ENTRETIENPREPAYE, Type = String, Dynamic = True, Default = \"Entretien pr\xC3\xA9p.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Entretien pr\xC3\xA9p."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Prepaid maint."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Entretien pr\xC3\xA9p."
	#tag EndConstant

	#tag Constant, Name = EQUIPEMENT, Type = String, Dynamic = True, Default = \"\xC3\x89quipement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89quipement"
	#tag EndConstant

	#tag Constant, Name = EQUIPEMENTPREFIX, Type = String, Dynamic = True, Default = \"equipm", Scope = Public
	#tag EndConstant

	#tag Constant, Name = EQUIPEMENTTABLENAME, Type = String, Dynamic = True, Default = \"equipm", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = ETATACTUEL, Type = String, Dynamic = True, Default = \"\xC3\x89tat actuel", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89tat actuel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Actual status"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89tat actuel"
	#tag EndConstant

	#tag Constant, Name = ETATACTUELDATE, Type = String, Dynamic = True, Default = \"\xC3\x89tat actuel date", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"\xC3\x89tat actuel date"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Actual status date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89tat actuel date"
	#tag EndConstant

	#tag Constant, Name = GARANTIEPROLONGEE, Type = String, Dynamic = True, Default = \"Garantie prol.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Garantie prol."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Ext. warranty"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Garantie prol."
	#tag EndConstant

	#tag Constant, Name = HINTADD, Type = String, Dynamic = True, Default = \"Ajouter", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ajouter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ajouter"
	#tag EndConstant

	#tag Constant, Name = HINTLOCKUNLOCK, Type = String, Dynamic = False, Default = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lock/Unlock record"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
	#tag EndConstant

	#tag Constant, Name = HINTSUPPRIMER, Type = String, Dynamic = True, Default = \"Supprimer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
	#tag EndConstant

	#tag Constant, Name = HINTUPDATEDETAILS, Type = String, Dynamic = False, Default = \"Enregistrer les modifications.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrer les modifications."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Save modifications."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrer les modifications."
	#tag EndConstant

	#tag Constant, Name = IMMATSAAQ, Type = String, Dynamic = True, Default = \"Immatriculation SAAQ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Immatriculation SAAQ"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"SAAQ registration"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Immatriculation SAAQ"
	#tag EndConstant

	#tag Constant, Name = INSPECTEUR, Type = String, Dynamic = True, Default = \"Inspecteur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Inspecteur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Inspector"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inspecteur"
	#tag EndConstant

	#tag Constant, Name = INSPECTION, Type = String, Dynamic = True, Default = \"Inspection", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Inspection"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Inspection"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inspection"
	#tag EndConstant

	#tag Constant, Name = KMEXC, Type = String, Dynamic = True, Default = \"Km exc.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Km exc."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Exc. km"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Km exc."
	#tag EndConstant

	#tag Constant, Name = KMHEURES, Type = String, Dynamic = True, Default = \"Km/ Heures", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Km/ Heures"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Km/ Hours"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Km/ Heures"
	#tag EndConstant

	#tag Constant, Name = KMPERMIS, Type = String, Dynamic = True, Default = \"Km permis", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Km permis"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Allowed km"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Km permis"
	#tag EndConstant

	#tag Constant, Name = LISTE, Type = String, Dynamic = True, Default = \"Liste", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Liste"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"List"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Liste"
	#tag EndConstant

	#tag Constant, Name = LOCALISATION, Type = String, Dynamic = True, Default = \"Localisation", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Localisation"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Location"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Localisation"
	#tag EndConstant

	#tag Constant, Name = LOCALISATIONDATE, Type = String, Dynamic = True, Default = \"Date de fin", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de fin"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de fin"
	#tag EndConstant

	#tag Constant, Name = LOCATION, Type = String, Dynamic = True, Default = \"Location", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Location"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Rent"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Location"
	#tag EndConstant

	#tag Constant, Name = LOYERMENSUEL, Type = String, Dynamic = True, Default = \"Loyer mensuel", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Loyer mensuel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Monthly rent"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Loyer mensuel"
	#tag EndConstant

	#tag Constant, Name = MARQUE, Type = String, Dynamic = True, Default = \"Marque", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Marque"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Brand"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Marque"
	#tag EndConstant

	#tag Constant, Name = MARQUEPNEU, Type = String, Dynamic = True, Default = \"Marque pneu", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Marque pneu"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Tire brand"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Marque pneu"
	#tag EndConstant

	#tag Constant, Name = MASSENETTE, Type = String, Dynamic = True, Default = \"Masse nette", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Masse nette"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Net mass"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Masse nette"
	#tag EndConstant

	#tag Constant, Name = MODELE, Type = String, Dynamic = True, Default = \"Mod\xC3\xA8le", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Mod\xC3\xA8le"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Model"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mod\xC3\xA8le"
	#tag EndConstant

	#tag Constant, Name = MOTCLE, Type = String, Dynamic = True, Default = \"Mot cl\xC3\xA9\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Mot cl\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Keyword"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Mot cl\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = MOTEUR, Type = String, Dynamic = True, Default = \"Moteur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Moteur"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Moteur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Motor"
	#tag EndConstant

	#tag Constant, Name = NOMBREDEMOIS, Type = String, Dynamic = True, Default = \"Nombre de mois", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nombre de mois"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Number of months"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nombre de mois"
	#tag EndConstant

	#tag Constant, Name = NOPLAQUE, Type = String, Dynamic = True, Default = \"No. plaque", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No. plaque"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Plate #"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"No. plaque"
	#tag EndConstant

	#tag Constant, Name = NOSERIE, Type = String, Dynamic = True, Default = \"No. s\xC3\xA9rie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No. s\xC3\xA9rie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Serial #"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"No. s\xC3\xA9rie"
	#tag EndConstant

	#tag Constant, Name = NOTHING, Type = String, Dynamic = True, Default = \"Rien", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Rien"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Nothing"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Rien"
	#tag EndConstant

	#tag Constant, Name = NOUNITEEXISTEDEJA, Type = String, Dynamic = True, Default = \"No. unit\xC3\xA9 existe d\xC3\xA9j\xC3\xA0 dans la base de donn\xC3\xA9es.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No. unit\xC3\xA9 existe d\xC3\xA9j\xC3\xA0 dans la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"No. unit\xC3\xA9 existe d\xC3\xA9j\xC3\xA0 dans la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Unit# already exists in database."
	#tag EndConstant

	#tag Constant, Name = POIDSPNVB, Type = String, Dynamic = True, Default = \"Poids PNVB", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Poids PNVB"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"PNVB weight"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Poids PNVB"
	#tag EndConstant

	#tag Constant, Name = PROJET, Type = String, Dynamic = True, Default = \"Projet", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Projet"
	#tag EndConstant

	#tag Constant, Name = PURCHASEORDERNO, Type = String, Dynamic = True, Default = \"No. bon de commande", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"#Bon de commande"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"#Purchase Order"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"#Bon de commande"
	#tag EndConstant

	#tag Constant, Name = REQUIS, Type = String, Dynamic = True, Default = \"Required", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Requis"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required"
	#tag EndConstant

	#tag Constant, Name = REVISE, Type = String, Dynamic = True, Default = \"R\xC3\xA9vis\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vis\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Reviewed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vis\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = REVISION, Type = String, Dynamic = True, Default = \"R\xC3\xA9vision", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vision"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Revision"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vision"
	#tag EndConstant

	#tag Constant, Name = SELECTIONRECORDADETRUIRE, Type = String, Dynamic = True, Default = \"S\xC3\xA9lectionner au moins un enregistrement \xC3\xA0 d\xC3\xA9truire.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"S\xC3\xA9lectionner au moins un enregistrement \xC3\xA0 d\xC3\xA9truire."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"S\xC3\xA9lectionner au moins un enregistrement \xC3\xA0 d\xC3\xA9truire."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select at least one record to delete."
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Statut", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = TYPEEQUIPEMENT, Type = String, Dynamic = True, Default = \"Type d\'\xC3\xA9quipement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Type d\'\xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment type"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Type d\'\xC3\xA9quipement"
	#tag EndConstant

	#tag Constant, Name = UNITENO, Type = String, Dynamic = True, Default = \"#Unit\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"#Unit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Unit#"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"#Unit\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = UNITENOCOMMENCEPART, Type = String, Dynamic = True, Default = \"#Unit\xC3\xA9 doit commencer par T-", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"#Unit\xC3\xA9 doit commencer par T-"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Unit# must begin by T-"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"#Unit\xC3\xA9 doit commencer par T-"
	#tag EndConstant

	#tag Constant, Name = VALEUR, Type = String, Dynamic = True, Default = \"Valeur\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Valeur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Value"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Valeur"
	#tag EndConstant

	#tag Constant, Name = VALEURRESIDUELLE, Type = String, Dynamic = True, Default = \"Valeur r\xC3\xA9siduelle", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Valeur r\xC3\xA9siduelle"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Residual value"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Valeur r\xC3\xA9siduelle"
	#tag EndConstant

	#tag Constant, Name = ZONEDOITETRENUMERIQUE, Type = String, Dynamic = True, Default = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This field must be a numeric value"
	#tag EndConstant

	#tag Constant, Name = ZONESOBLIGATOIRES, Type = String, Dynamic = True, Default = \"Required field(s)", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required field(s)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Zone(s) obligatoire(s)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required field(s)"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
