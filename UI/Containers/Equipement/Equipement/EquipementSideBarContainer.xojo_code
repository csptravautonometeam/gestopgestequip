#tag WebPage
Begin WebContainer EquipementSideBarContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   550
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1100
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle EquipRectangle
      Cursor          =   0
      Enabled         =   True
      Height          =   550
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1622472703"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox EquipementListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   12
      ColumnWidths    =   "5%,15%,8%,20%,7%,7%,7%,5%,5%,7%,7%,7%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   469
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "#EquipementModule.UNITENO	Description	#EquipementModule.TYPEEQUIPEMENT	#EquipementModule.LOCALISATION	#EquipementModule.LOCALISATIONDATE	#EquipementModule.BAILEXP	#EquipementModule.DATEEXPSAAQ	#EquipementModule.KMHEURES	#EquipementModule.KMPERMIS	#EquipementModule.CERTIFCALIBDATE	#EquipementModule.CERTIFCALIBDATEDUE	"
      Left            =   5
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "0"
      TabOrder        =   -1
      Top             =   80
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1090
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin CritereContainerControl EquipementSideBarContainerCritereContainerControl
      Cursor          =   0
      Enabled         =   True
      Height          =   80
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   0
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   894
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModEquipement
      AlignHorizontal =   0
      AlignVertical   =   3
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   926
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1064464383
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelEquipement
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   971
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   680198143
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddEquipement
      AlignHorizontal =   2
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1005
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   688117759
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVRafEquipement
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1050
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   644532223
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Shown()
		  populateEquipement
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ajouterEquipement()
		  Dim compteur As Integer
		  compteur = EquipementListBox.RowCount  'Pour Debug
		  compteur = EquipementListBox.LastIndex    'Pour Debug
		  compteur = EquipementListBox.ListIndex ' Pour Debug
		  
		  //EquipementListBox.insertRow(compteur+1, "T-00-0000", "toeo", Self.sEquipement.equipm_localisation_date, , "A")
		  // Faire pointer le curseur sur le nouveau projet
		  //EquipementListBox.ListIndex = EquipementListBox.ListIndex + 1
		  //Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne = EquipementListBox.ListIndex
		  EquipementListBox.ListIndex = 0
		  Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne = EquipementListBox.ListIndex
		  
		  Session.pointeurEquipementSideBarContainer.equipementStruct.cle = "T-"
		  Session.pointeurEquipementSideBarContainer.equipementStruct.type = "toeo"
		  Session.pointeurEquipementSideBarContainer.equipementStruct.statut = "A"
		  Session.pointeurEquipementSideBarContainer.equipementStruct.edit_mode = "Creation"
		  
		  // Création d'un nouvel enregistrement d'équipement
		  Dim indexCritere As Integer = EquipementSideBarContainerCritereContainerControl.EquipementTypeEquipementPopupMenu.ListIndex
		  Dim classCritere As String = "toeo"
		  If indexCritere <= 0 Then
		    Self.sEquipement = new AllEquipmentToolClass()
		  Else
		    classCritere = EquipementSideBarContainerCritereContainerControl.EquipementTypeEquipementPopupMenu.RowTag(indexCritere)
		    Self.sEquipement = selectEquipementClass(classCritere)
		  End If
		  Self.sEquipement.equipm_unite_no = "T-"
		  Self.sEquipement.equipm_type = classCritere
		  Dim dateTrav As Date = new Date()
		  Self.sEquipement.equipm_localisation_date = str(dateTrav.Year) + "-" +str(dateTrav.Month) + "-" + str(dateTrav.Day)
		  Self.sEquipement.equipm_statut = "A"
		  
		  Session.pointeurEquipementSideBarContainer.equipementStruct.id = 0  // indique que c'est un nouvel enregistrement
		  Self.sEquipement.equipm_id = 0
		  Dim ML6EquipementWebDialog As EquipementWebDialog = New EquipementWebDialog
		  ML6EquipementWebDialog.EquipementContainerDialog.majEquipementDetail
		  ML6EquipementWebDialog = Nil
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  populateEquipement
		  
		  // Se mettre en mode lock
		  implementLock
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementEquipement(idEquipement_param As Integer)
		  // Si la valeur de idEquipement  est 0, c'est un nouvel enregistrement, donc pas de lecture de la BD
		  If idEquipement_param  = 0 Then
		    Session.pointeurEquipementSideBarContainer.equipementStruct.edit_mode = "Creation"
		  Else
		    Self.sEquipement = Nil
		    // Passer le type d'équipement en paramètre
		    Self.sEquipement = selectEquipementClass(EquipementListBox.CellTag(EquipementListBox.ListIndex,2))
		    Dim stest As EquipementClass = Self.sEquipement
		    // Lecture
		    Self.sEquipement.listDataByField(Session.bdTechEol, EQUIPEMENTTABLENAME, equipementprefix,  _
		    "equipm_id", str(Session.pointeurEquipementSideBarContainer.equipementStruct.id), "equipm_id")
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  //Super.Constructor
		  
		  // Écraser les paramètres par défaut de la classe
		  
		  //Me.Width = 1200
		  //Me.LockLeft = True
		  //Me.LockRight = True
		  //Me.LockTop = True
		  //Me.LockBottom = True
		  //Me.Top = 0
		  //Me.Left = 0 
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub implementLock()
		  Dim message As String = ""
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim equip_recherche As String = EquipementModule.DBSCHEMANAME + "." + EquipementModule.DBTABLENAME + "." + str(Self.sEquipement.equipm_id)
		  message = Self.sEquipement.check_If_Locked(Session.bdTechEol, equip_recherche)
		  If  message <> "Locked" And message <> "Unlocked" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If Message = "Locked" Then
		    message = EquipementModule.ENREGVERROUILLEPAR + Self.sEquipement.lockedBy
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  //Activer le mode Verrouillage
		  Self.sEquipement.lockEditMode = True
		  
		  //Creer les valeurs de verrouillage record ID
		  Self.sEquipement.recordID = EquipementModule.DBSCHEMANAME + "." +EquipementModule.DBTABLENAME+ "." + str(Self.sEquipement.equipm_id)
		  message = Self.sEquipement.lockRow(Session.bdTechEol) 
		  If message <> "Succes" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  Dim ML6EquipementWebDialog As EquipementWebDialog = New EquipementWebDialog
		  // Appeler la modification
		  ML6EquipementWebDialog.EquipementContainerDialog.lisEquipementDetail
		  ML6EquipementWebDialog.Show
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub loadImageLogo(image as WebImageView)
		  
		  //image.Picture = CartierRepPalesLogo350x100
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modifierEquipement()
		  //Déclenchement du mode édition et déverrouillage ou verrouillage
		  If EquipementListBox.ListIndex <> -1 And EquipementListBox.RowCount > 0 Then
		    implementLock
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateEquipement(Optional equipm_unite_no_param as String)
		  If Self.sEquipement = Nil Then
		    Self.sEquipement = new EquipementClass()
		  End If
		  
		  Dim listeSelection As Dictionary = EquipementSideBarContainerCritereContainerControl.chargementListeSelection
		  
		  Dim equipementRS As RecordSet
		  equipementRS = Self.sEquipement.loadDataEquipSequence(Session.bdTechEol, listeSelection)
		  
		  If equipementRS <> Nil Then
		    EquipementListBox.DeleteAllRows
		    
		    For i As Integer = 1 To equipementRS.RecordCount
		      EquipementListBox.AddRow(equipementRS.Field("equipm_unite_no").StringValue, equipementRS.Field("equipm_description").StringValue, _
		      equipementRS.Field("equipm_type_description").StringValue, equipementRS.Field("equipm_localisation_description").StringValue, _
		      equipementRS.Field("equipm_localisation_date").StringValue, equipementRS.Field("equipm_locat_date_fin").StringValue, _
		      equipementRS.Field("equipm_inspect_date_saaq_exp").StringValue, equipementRS.Field("equipm_km_heures").StringValue, _
		      equipementRS.Field("equipm_locat_km_permis").StringValue, equipementRS.Field("equipm_certcalib_date").StringValue, _
		      equipementRS.Field("equipm_certcalib_date_due").StringValue)
		      
		      If equipementRS.Field("equipm_statut").StringValue <> "A" Then
		        For j As Integer = 0 To 4
		          EquipementListBox.CellStyle(i-1, j) = BGGray200
		        Next
		      End If
		      
		      // Garder le numéro de ligne
		      EquipementListBox.RowTag(EquipementListBox.LastIndex) = i -1
		      // Garder l'id numéro du equipement
		      EquipementListBox.CellTag(EquipementListBox.LastIndex,1) = equipementRS.Field("equipm_id").IntegerValue
		      // Garder le type d'équipement
		      EquipementListBox.CellTag(EquipementListBox.LastIndex,2) = equipementRS.Field("equipm_type").StringValue
		      // Garder le type d'équipement
		      EquipementListBox.CellTag(EquipementListBox.LastIndex,3) = equipementRS.Field("equipm_unite_no").StringValue
		      
		      equipementRS.MoveNext
		    Next
		    
		    equipementRS.Close
		    equipementRS = Nil
		    
		    // Positionnement par rapport à un numéro d'unité
		    If equipm_unite_no_param <> "" Then
		      For indexListBox As Integer = 0 To EquipementListBox.RowCount
		        If EquipementListBox.CellTag(indexListBox,3) = equipm_unite_no_param Then
		          Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne = indexListBox
		          EquipementListBox.ListIndex  = indexListBox
		          Session.pointeurEquipementSideBarContainer.equipementStruct.id = EquipementListBox.CellTag(Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne,1)
		          Exit Sub
		        End If
		      Next
		      Exit Sub
		    End If
		    
		    // Positionnement à la ligne gardée en mémoire si la liste n'est pas vide
		    If EquipementListBox.ListIndex  <> Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne And EquipementListBox.RowCount > 0 Then
		      Dim v As Variant = EquipementListBox.LastIndex
		      v = Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne
		      If Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne > EquipementListBox.LastIndex  Then Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne = EquipementListBox.LastIndex
		      EquipementListBox.ListIndex  = Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne
		      Session.pointeurEquipementSideBarContainer.equipementStruct.id = EquipementListBox.CellTag(Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne,1)
		    End If
		  End If
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function selectEquipementClass(typeEquipement As String) As EquipementClass
		  Dim equipementClass As EquipementClass
		  
		  Select Case typeEquipement 
		    
		  Case "voit"
		    Dim voitureClassInst As VoitureClass = new VoitureClass
		    equipementClass = voitureClassInst
		    
		  Case "cami"
		    Dim camionClassInst As CamionClass = new CamionClass
		    equipementClass = camionClassInst
		    
		  Case "civi", "tsgs"
		    Dim civCoulisseauClassInst As CivCoulisseauClass = new CivCoulisseauClass
		    equipementClass = civCoulisseauClassInst
		    
		  Case "detc", "durb", "durs", "erad", "mumg", "piam", "pmph", "thha", "thin", "towr", "maal"
		    Dim eqOutilSelClassInst As EqOutilSelClass = new EqOutilSelClass
		    equipementClass = eqOutilSelClassInst
		    
		  Case "kits", "pltf"
		    Dim kitPltfClassInst As KitPltfClass = new KitPltfClass
		    equipementClass = kitPltfClassInst
		    
		  Case "remo", "remf"
		    Dim remClassInst As RemClass = new RemClass
		    equipementClass = remClassInst
		    
		  Case "damu"
		    Dim damClassInst As DamClass = new DamClass
		    equipementClass = damClassInst
		    
		  Case "pelj", "defb", "extc"
		    Dim peDeExClassInst As PeDeExClass = new PeDeExClass
		    equipementClass = peDeExClassInst
		    
		  Case "bore", "trai"
		    Dim borTraiClassInst As BorTraiClass = new BorTraiClass
		    equipementClass = borTraiClassInst
		    
		  Case "gene"
		    Dim genClassInst As GenClass = new GenClass
		    equipementClass = genClassInst
		    
		  Case "motn"
		    Dim motClassInst As MotClass = new MotClass
		    equipementClass = motClassInst
		    
		  Case "trec"
		    Dim tourClassInst As TourClass = new TourClass
		    equipementClass = tourClassInst
		    
		  Case "vrad", "tens"
		    Dim vradTensClassInst As VradTensClass = new VradTensClass
		    equipementClass = vradTensClassInst
		    
		  Case "toeo"
		    Dim allEquipmentToolClassInst As AllEquipmentToolClass = new AllEquipmentToolClass
		    equipementClass = allEquipmentToolClassInst
		    
		  Case Else // par défaut
		    Dim equiClass As EquipementClass = new EquipementClass
		    equipementClass = equiClass
		    
		  End Select
		  
		  Return equipementClass
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub supEquipementDetail()
		  
		  // Vérifier si un enregistrement peut être supprimé
		  Dim messageErreur as String = ""
		  messageErreur =  Self.sEquipement.validerSuppression(Session.bdTechEol, Self.sEquipement.equipm_unite_no)
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = EquipementModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim equip_recherche As String = EquipementModule.DBSCHEMANAME + "." + EquipementModule.DBTABLENAME + "." + str(Self.sEquipement.equipm_id)
		  messageErreur = Self.sEquipement.check_If_Locked(Session.bdTechEol, equip_recherche)
		  If  messageErreur <> "Locked" And messageErreur <> "Unlocked" Then
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If messageErreur = "Locked" Then
		    messageErreur = EquipementModule.ENREGVERROUILLEPAR + Self.sEquipement.lockedBy
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  
		  // L'id  est contenu dans la variable Session.pointeurEquipementSideBarContainer.equipementStruct.id
		  messageErreur = Self.sEquipement.supprimerDataByField(Session.bdTechEol, EquipementModule.EQUIPEMENTTABLENAME, "equipm_id",str(Session.pointeurEquipementSideBarContainer.equipementStruct.id))
		  
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = EquipementModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		  Else
		    // Renuméroter et Réafficher la liste des projets
		    Self.populateEquipement
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub unlock()
		  Dim lockedRowIDBis As integer = Session.sauvegardeLockedRowID
		  Dim message As String = Self.sEquipement.unLockRow(Session.bdTechEol)
		  If  message <> "Succes" Then
		    afficherMessage(message)
		  End If
		  
		  Self.sEquipement.lockEditMode = False
		  Self.Close
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		equipementStruct As equipementStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		sEquipement As EquipementClass
	#tag EndProperty


	#tag Structure, Name = equipementStructure, Flags = &h0
		cle As String*30
		  id As Integer
		  no_ligne As Integer
		  edit_mode As String*30
		  table_nom As String*30
		  statut As String*1
		type As String*4
	#tag EndStructure


#tag EndWindowCode

#tag Events EquipementListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Ancun equipement n'a été sélectionné, réinitialiser les champs
		    //ClearEquipementsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    Session.pointeurEquipementSideBarContainer.equipementStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    Session.pointeurEquipementSideBarContainer.equipementStruct.id = Me.CellTag(Me.ListIndex,1)
		    changementEquipement(Me.CellTag(Me.ListIndex,1))
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub DoubleClick(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  IVModEquipement.Picture = ModModal30x30
		  IVAddEquipement.Picture = AjouterModal30x30
		  IVRafEquipement.Picture = RefreshMenu30x30
		  IVDelEquipement.Picture = DeleteModal30x30
		  
		  modifierEquipement
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModEquipement
	#tag Event
		Sub MouseExit()
		  Me.Picture = ModModal30x30
		  IVAddEquipement.Picture = AjouterModal30x30
		  IVRafEquipement.Picture = RefreshMenu30x30
		  IVDelEquipement.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Picture = ModModalOrange30x30
		  IVAddEquipement.Picture = AjouterModal30x30
		  IVRafEquipement.Picture = RefreshMenu30x30
		  IVDelEquipement.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  Me.Picture = ModModal30x30
		  IVAddEquipement.Picture = AjouterModal30x30
		  IVRafEquipement.Picture = RefreshMenu30x30
		  IVDelEquipement.Picture = DeleteModal30x30
		  
		  modifierEquipement
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelEquipement
	#tag Event
		Sub MouseEnter()
		  If Me.Enabled = True Then
		    Me.Picture = DeleteModalOrange
		    IVAddEquipement.Picture = AjouterModal30x30
		    IVModEquipement.Picture = ModModal30x30
		    IVRafEquipement.Picture = RefreshMenu30x30
		  End If
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  supEquipementDetail
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Me.Picture = DeleteModal30x30
		  IVAddEquipement.Picture = AjouterModal30x30
		  IVModEquipement.Picture = ModModal30x30
		  IVRafEquipement.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddEquipement
	#tag Event
		Sub MouseEnter()
		  If Me.Enabled = True Then
		    Me.Picture = AjouterModalOrange30x30
		    IVRafEquipement.Picture = RefreshMenu30x30
		    IVModEquipement.Picture = ModModal30x30
		    IVDelEquipement.Picture = DeleteModal30x30
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ajouterEquipement
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Me.Picture = AjouterModal30x30
		  IVRafEquipement.Picture = RefreshMenu30x30
		  IVModEquipement.Picture = ModModal30x30
		  IVDelEquipement.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVRafEquipement
	#tag Event
		Sub MouseExit()
		  Me.Picture = RefreshMenu30x30
		  IVAddEquipement.Picture = AjouterModal30x30
		  IVModEquipement.Picture = ModModal30x30
		  IVDelEquipement.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  If Me.Enabled = True Then
		    Me.Picture = RefreshMenuOra30x30
		    IVAddEquipement.Picture = AjouterModal30x30
		    IVModEquipement.Picture = ModModal30x30
		    IVDelEquipement.Picture = DeleteModal30x30
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  populateEquipement
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
