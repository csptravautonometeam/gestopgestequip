<?php


/**
 * Form Processing Configurations
 */
/**
 * Global set up
 */
	$year_month_day = date('Ymd'); // Displays 20100228

/**
 * PostGres database set up
 */
	$postgresql = true; // true or false ('true' is to use database, 'false' does not)
	$postgresql_host = 'localhost';
	$fileTest = dirname( dirname(__FILE__)).'\\gestequip\\ProdOuTest.ini';
	if(file_exists($fileTest))
	{
		// On est en test
		$postgresql_host = '192.168.0.51';
	}
	$postgresql_db_name = 'techeol'; // database name
	$postgresql_user_name = 'admin'; // user name to database
	$postgresql_password = 'postgres'; // password for user

/**
 * Do PostGres, if set to 'true'
 */
	postgreSQLConnection($postgresql_host,$postgresql_user_name,$postgresql_password,$postgresql_db_name);
	pg_set_client_encoding("UTF-8");
	//pg_set_client_encoding("WIN1252");

	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    echo "<database>";

/*  Mise � jour de la table des NIP	*/

	$sql = "SELECT * FROM portail.portail_user where portail_user_nip is not null AND portail_user_nip <> '' AND portail_user_status = '1' order by portail_user_id";
	$result = pg_query($sql) or die(pg_last_error()); //executes query

    echo "  <table id=\"niptable\" updated=\"".date('Y-m-d H:i:s', strtotime('+1 days'))." +0000\">\n";
    echo "       <thead>\n";
    echo "          <tr>\n";
    echo "              <th type=\"id\">id</th>\n";
    echo "              <th type=\"string\">nip</th>\n";
    echo "              <th type=\"string\">nom_employe</th>\n";
    echo "          </tr>\n";
    echo "       </thead>\n";
    echo "       <tbody>\n";

    while($row = pg_fetch_array($result))
    {
        echo "          <tr>\n";
        echo "              <td>".$row['portail_user_id']."</td>\n";
        echo "              <td>".repl_carac_franc($row['portail_user_nip'])."</td>\n";
        echo "              <td>".repl_carac_franc($row['portail_user_firstname'])." ".repl_carac_franc($row['portail_user_lastname'])."</td>\n";
        echo "          </tr>\n";
	}

    echo "      </tbody>\n";
    echo "  </table>\n";

/*  Mise � jour de la table des employes */

	//$sql = "SELECT * FROM dbglobal.employe where portail_user_status = '1' order by portail_user_id";
	$sql = "SELECT * FROM dbglobal.employe order by employe_id";
	$result = pg_query($sql) or die(pg_last_error()); //executes query

    echo "  <table id=\"employe\" updated=\"".date('Y-m-d H:i:s', strtotime('+1 days'))." +0000\">\n";
    echo "       <thead>\n";
    echo "          <tr>\n";
    echo "              <th type=\"id\">id</th>\n";
    echo "              <th type=\"string\">employe_id</th>\n";
    echo "              <th type=\"string\">nom_employe</th>\n";
    echo "          </tr>\n";
    echo "       </thead>\n";
    echo "       <tbody>\n";

    while($row = pg_fetch_array($result))
    {
        echo "          <tr>\n";
        echo "              <td>".$row['employe_id']."</td>\n";
        echo "              <td>".repl_carac_franc($row['employe_id'])."</td>\n";
        echo "              <td>".repl_carac_franc($row['employe_nom'])." ".repl_carac_franc($row['employe_prenom'])."</td>\n";
        echo "          </tr>\n";
	}

    echo "      </tbody>\n";
    echo "  </table>\n";


/*  Mise � jour de la table des �quipements */

	$sql = "SELECT *, ";
	$sql = $sql."(CASE WHEN equipm_marque Is Null Then '' Else equipm_marque || ' ' END || ";
	$sql = $sql." CASE WHEN equipm_modele Is Null Then ' ' Else equipm_modele || ' ' END ||";
	$sql = $sql." CASE WHEN equipm_annee    Is Null Then ' ' Else equipm_annee   || ' ' END ||";
	$sql = $sql." CASE WHEN equipm_couleur Is Null Then ' ' Else equipm_couleur || ' ' END ||";
	$sql = $sql." CASE WHEN equipm_moteur Is Null Then ' ' Else equipm_moteur  || ' ' END) ";
    $sql = $sql." As equipm_description, ";
    $sql = $sql." (select inspveh_dommages_connus  from form.inspection_vehicule where inspection_vehicule.inspveh_no_unite = equipm.equipm_unite_no ORDER BY inspection_vehicule.created DESC Limit 1)  as inspveh_dommages_connus ";
	$sql = $sql."FROM equip.equipm ";
	$sql = $sql."where equipm_type in('pltf') and equipm_statut = 'A' order by equipm_unite_no ";
	$result = pg_query($sql) or die(pg_last_error()); //executes query

    echo "  <table id=\"plateforme\" updated=\"".date('Y-m-d H:i:s', strtotime('+1 days'))." +0000\">\n";
    echo "       <thead>\n";
    echo "          <tr>\n";
    echo "              <th type=\"id\">id</th>\n";
    echo "              <th type=\"string\">no_plateforme</th>\n";
    echo "              <th type=\"string\">desc_plateforme</th>\n";
    echo "              <th type=\"string\">dern_heures</th>\n";
    echo "          </tr>\n";
    echo "       </thead>\n";
    echo "       <tbody>\n";

    while($row = pg_fetch_array($result))
    {
        echo "          <tr>\n";
        echo "              <td>".$row['equipm_id']."</td>\n";
        echo "              <td>".repl_carac_franc($row['equipm_unite_no'])."</td>\n";
        echo "              <td>".repl_carac_franc($row['equipm_description'])."</td>\n";
        echo "              <td>".repl_carac_franc($row['equipm_km_heures'])."</td>\n";
        echo "          </tr>\n";
	}

    echo "      </tbody>\n";
    echo "  </table>\n";


/*  Mise � jour de la table des projets */

	$sql = "SELECT * FROM dbglobal.projet WHERE depot_note = 'Travaux en cours'  Order By projet_no";
	$result = pg_query($sql) or die(pg_last_error()); //executes query

    echo "  <table id=\"projet\" updated=\"".date('Y-m-d H:i:s', strtotime('+1 days'))." +0000\">\n";
    echo "       <thead>\n";
    echo "          <tr>\n";
    echo "              <th type=\"id\">id</th>\n";
    echo "              <th type=\"string\">no_projet</th>\n";
    echo "              <th type=\"string\">desc_projet</th>\n";
    echo "          </tr>\n";
    echo "       </thead>\n";
    echo "       <tbody>\n";

    while($row = pg_fetch_array($result))
    {
        echo "          <tr>\n";
        echo "              <td>".$row['projet_id']."</td>\n";
        echo "              <td>".repl_carac_franc($row['projet_no'])."</td>\n";
        echo "              <td>".repl_carac_franc($row['projet_no'])." ".repl_carac_franc($row['projet_titre'])."</td>\n";
        echo "          </tr>\n";
	}

    echo "      </tbody>\n";
    echo "  </table>\n";



    echo "</database>\n";


	pg_close();


function postgreSQLConnection($postgresql_host,$postgresql_user_name,$postgresql_password,$postgresql_db_name)
{
	$connectParam = 'host='.$postgresql_host.' port=5432 dbname='.$postgresql_db_name.' user='.$postgresql_user_name.' password='.$postgresql_password;
	if (!pg_connect($connectParam)) {
		xml_response('false','true','Remote Server Error','Could not select the database in PostgreSQL. Please try again later.','OK');
		exit();
	}
}

/**
 * Remplacer les caract�res fran�ais
 *
 */
	function repl_carac_franc($chaine)
	{

        $chaine_trav = $chaine;
/*
	    $chaine_trav = str_replace("�", "a", $chaine_trav);
	    $chaine_trav = str_replace("�", "a", $chaine_trav);
	    $chaine_trav = str_replace("�", "e", $chaine_trav);
	    $chaine_trav = str_replace("�", "e", $chaine_trav);
	    $chaine_trav = str_replace("�", "e", $chaine_trav);
	    $chaine_trav = str_replace("�", "e", $chaine_trav);
	    $chaine_trav = str_replace("�", "i", $chaine_trav);
	    $chaine_trav = str_replace("�", "o", $chaine_trav);
	    $chaine_trav = str_replace("�", "u", $chaine_trav);
	    $chaine_trav = str_replace("�", "u", $chaine_trav);
	    $chaine_trav = str_replace("�", "c", $chaine_trav);

	    $chaine_trav = str_replace("�", "A", $chaine_trav);
	    $chaine_trav = str_replace("�", "A", $chaine_trav);
	    $chaine_trav = str_replace("�", "E", $chaine_trav);
	    $chaine_trav = str_replace("�", "E", $chaine_trav);
	    $chaine_trav = str_replace("�", "E", $chaine_trav);
	    $chaine_trav = str_replace("�", "E", $chaine_trav);
	    $chaine_trav = str_replace("�", "I", $chaine_trav);
	    $chaine_trav = str_replace("�", "O", $chaine_trav);
	    $chaine_trav = str_replace("�", "U", $chaine_trav);
	    $chaine_trav = str_replace("�", "U", $chaine_trav);
	    $chaine_trav = str_replace("�", "C", $chaine_trav);


	    $chaine_trav = str_replace("�", "&#224;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#226;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#233;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#232;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#234;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#235;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#238;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#244;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#249;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#251;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#231;", $chaine_trav);

	    $chaine_trav = str_replace("�", "&#192;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#194;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#201;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#200;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#202;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#203;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#206;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#212;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#217;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#219;", $chaine_trav);
	    $chaine_trav = str_replace("�", "&#199;", $chaine_trav);
*/


		return $chaine_trav;
	}
/**
 * Send XML Response.
 *
 * @param string $success String for success or failure ('true' is success, 'false' is failure)
 * @param string $show_alert String for showing alert on FormEntry Touch ('true' to show, 'false' to not show)
 * @param string $title String for displaying title of alert on FormEntry Touch
 * @param string $message String for displaying body message of alert on FormEntry Touch
 * @param string $button_label String for displaying button label of alert on FormEntry Touch
 */
	function xml_response($success = 'false', $show_alert = 'true', $title = 'Not Successful', $message = 'The post was not successfully.', $button_label = 'OK') {
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo "<response>\n";
		echo "	<success>".$success."</success>\n";
		echo "	<show_alert>".$show_alert."</show_alert>\n";
		echo "	<title>".$title."</title>\n";
		echo "	<message>".$message."</message>\n";
		echo "	<button_label>".$button_label."</button_label>\n";
		echo "</response>\n";
	}


?>