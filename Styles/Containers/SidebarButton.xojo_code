#tag WebStyle
WebStyle SidebarButton
Inherits WebStyle
	#tag WebStyleStateGroup
		text-color=2F3A56FF
		shadow-text=1px 0px 0px FFFFFF7F
		misc-background=gradient vertical 0 B3C5DEFF 1 6B839EFF
		corner-topleft=4px
		corner-bottomleft=4px
		corner-bottomright=4px
		corner-topright=4px
		shadow-box=1px 0px 0px FFFFFF7F
		border-top=1px solid 616E8EFF
		border-left=1px solid 616E8EFF
		border-bottom=1px solid 616E8EFF
		border-right=1px solid 616E8EFF
		text-size=11px
		text-decoration=True false false false false
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 6B839EFF 1 B3C5DEFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle SidebarButton
#tag EndWebStyle

